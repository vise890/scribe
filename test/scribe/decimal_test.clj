(ns scribe.decimal-test
  (:require [clojure.spec.alpha :as s]
            [clojure.test :refer :all]
            [scribe.decimal :as sut]))

;; TODO generative test the shit out of this

(defn set-precision-ok?
  [bd precision]
  (let [original-scale (.scale bd)
        act            (sut/set-precision bd precision)]
    (is (= bd act) "should not change the value of the BigDecimal")
    (is (= precision (.precision act))
        "should set the precision to the one specified")))

(deftest set-precision-test
  (set-precision-ok? 1.0000M 1)
  (set-precision-ok? 1M 5)
  (set-precision-ok? -0.42M 5)
  (is (thrown? ArithmeticException (sut/set-precision 0.123M 1))))

(defmacro is-spec-invalid
  [form]
  ;; ??? HACK i don't even wanna know why apparently (sometimes!) this is an
  ;; invalid let if you leave it a keyword
  ;;
  ;; 2018-04-28 After months of being fine, this resurfaced ಠ_ಠ
  `(is (= ":clojure.spec.alpha/invalid" (str ~form))))

(defn conforming-ok?
  [bd exp-precision exp-scale]
  (testing
   "conforming"
    (let [original-precision (.precision bd)
          original-scale     (.scale bd)
          act                (s/conform (sut/->decimal-conformer :precision exp-precision
                                                                 :scale exp-scale)
                                        bd)]
      (is (= bd act) "should not change the value")
      (is (= exp-scale (.scale act)) "should set the scale")
      (is (>= exp-precision (.precision act))
          "precision should be no more than exp-precision"))))

(deftest decimal-conformer-test
  (conforming-ok? 1.4200M 3 2)
  (conforming-ok? 1.42M 3 2)
  (conforming-ok? 1.4M 5 1)
  (conforming-ok? 1.4M 5 1)
  (testing "shoud return invalid for something that isn't a decimal"
    (let [c (sut/->decimal-conformer :precision 1
                                     :scale 1)]
      (is-spec-invalid (s/conform c 42))
      (is-spec-invalid (s/conform c 42.2))
      (is-spec-invalid (s/conform c :foo))))

  (testing "anything that requires rounding is invalid"
    ;; TODO *math-context* for rounding up/down
    (is-spec-invalid (s/conform (sut/->decimal-conformer :scale 1
                                                         :precision 3)
                                0.123M))
    (is-spec-invalid (s/conform (sut/->decimal-conformer :scale 3
                                                         :precision 1)
                                0.123M))))

(defn avro-serde-ok?
  [bd asserted-initial-precision asserted-initial-scale exp-precision exp-scale]
  (assert (= asserted-initial-precision (.precision bd))
          {:err      "supplied bd's precision doesn't match"
           :asserted asserted-initial-precision
           :found    (.precision bd)})
  (assert (= asserted-initial-scale (.scale bd))
          {:err      "supplied bd's scale doesn't match"
           :asserted asserted-initial-scale
           :found    (.scale bd)})
  (testing "avro serde"

    (testing "converting decimal<->bytes"
      (let [act (-> bd
                    (sut/decimal->bytes exp-precision exp-scale)
                    (sut/bytes->decimal exp-precision exp-scale))]
        (is (= bd act) "should not change the value")
        (is (= exp-scale (.scale act)) "should not change scale")))

    (testing "converting decimal<->fixed"
      ;; FIXME FAIL implement decimal<->fixed
      #_(is (= in
               (-> in
                   (sut/decimal->fixed exp-precision exp-scale)
                   (sut/fixed->decimal exp-precision exp-scale)))))))

(deftest avro-serde-test

  (testing "same same"
    (avro-serde-ok? 1.42M
                    3 2
                    3 2))

  (testing "precision going up"
    (avro-serde-ok? 1.42M
                    3 2
                    4 2))
  ;; TODO ??? can precision go down without scale going down?

  (testing "scale going up"
    (avro-serde-ok? 1.420M
                    4 3
                    4 2))
  (testing "scale going down"
    (avro-serde-ok? 1.420M
                    4 3
                    4 2))

  (testing "both going up"
    (avro-serde-ok? 1.4M
                    2 1
                    3 2))
  (testing "both going down"
    (avro-serde-ok? 0.10M
                    2 2
                    1 1))

  (testing "ArithmeticException thrown when need rounding"
    (is (thrown? java.lang.ArithmeticException
                 (sut/decimal->bytes 0.123M 3 2)))
    (is (thrown? java.lang.ArithmeticException
                 (sut/decimal->bytes 123.02M 4 2)))))
