(ns scribe.json-schema.specs-test
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.spec.alpha :as s]
            [clojure.test :refer :all]
            [expound.alpha :as expound]))
(require 'scribe.json-schema.specs)

(defonce k8s-openapi
  (-> "k8s_openapi.json"
      io/resource
      io/reader
      (json/parse-stream true)))

(def k8s-openapi-definitions
  (-> k8s-openapi
      :definitions
      vals))

(deftest k8s-defintions-test
  (doseq [d (->> k8s-openapi
                 :definitions
                 vals)]
    (is (s/valid? :JSONSchema/Schema d)
        (expound/expound-str :JSONSchema/Schema d))))
