(ns scribe.avro.schema-specs-test
  (:require [clojure.spec.alpha :as s]
            [clojure.test :refer :all]
            [scribe.avro.schema-specs :as as]
            [scribe.test-utils :as tu]))

(deftest names
  (doseq [n #{"MyN" "myNS" "a"}]
    (tu/is-valid :avro/name n))
  (doseq [ns #{"myNS.a" "my.ns" "_01.NS" "ns._0"}]
    (tu/is-valid :avro/namespace ns)))

(deftest primitive-types
  (tu/is-valid :avro/primitive "string")
  (tu/is-valid :avro/schema {:type "string"})
  (tu/is-valid :avro/schema {:type :string}))

(deftest records
  (tu/is-valid :avro/record {:type    "record"
                             :name    "LongList"
                             :aliases ["LinkedLongs"]
                             :fields  [{:name "value" :type "long"}
                                       {:name "next" :type ["null" "LongList"]}]})

  (testing "must not have empty fields"
    ;; .. lest the Scala lib crap itself
    (is (not (s/valid? :avro.record/fields [])))
    (is (not (s/valid? :avro/record
                       {:type   "record"
                        :name   "LongList"
                        :fields []})))
    (is (not (s/valid? :avro/record
                       {:name      "SN",
                        :namespace "ns",
                        :type      "record",
                        :fields
                        [{:name "someName", :type "string"}
                         {:name "items",
                          :type {:name      "Items",
                                 :namespace "ns.SN",
                                 :type      "record",
                                 :fields    []}}]})))))

(deftest enums
  (tu/is-valid :avro/enum {:type "enum" :name "Foo" :symbols ["foo" "bar"]})
  (tu/is-valid :avro/enum {:type :enum :name "Foo" :symbols #{:foo :bar}}))

(deftest arrays
  (tu/is-valid :avro/array {:type "array" :items "string"})
  (tu/is-valid :avro/array {:type "array" :items {:type "string"}})
  (tu/is-valid :avro/array {:type  "array"
                            :items {:type      "record"
                                    :name      "LongRecord"
                                    :namespace "me.vise890.avro.zomg"
                                    :fields    [{:name "value" :type "long"}]}}))

(deftest maps
  (tu/is-valid :avro/map {:type "map" :values "string"})
  (tu/is-valid :avro/map {:type "map" :values {:type "string"}})
  (tu/is-valid :avro/map {:type   "map"
                          :values {:type      "record"
                                   :name      "LongRecord"
                                   :namespace "me.vise890.avro.zomg"
                                   :fields    [{:name "value" :type "long"}]}}))

(deftest unions
  (tu/is-valid :avro/union [:null :string])
  (tu/is-valid :avro/union [{:name "foo"
                             :type :string}
                            {:name "bar"
                             :type :boolean}])
  (testing "cannot be empty"
    (is (not (s/valid? :avro/union [])))))

(deftest all-together
  ;; NOTE I don't know if this is actually valid
  (tu/is-valid ::as/avro-schema
               [{:type "array" :items {:type   "record"
                                       :name   "LongRecord"
                                       :fields [{:name "value" :type "long"}]}}
                "string"
                :null
                {:type "array" :items [:null
                                       {:type    "record"
                                        :name    "LongList"
                                        :aliases ["LinkedLongs"]
                                        :fields  [{:name "value" :type "long"}
                                                  {:name "next" :type ["null" "LongList"]}]}]}]))

(comment
  ;;;
  ;; Eval me to sanity check / debug!
  (s/conform ::as/avro-schema
             [{:type "array" :items {:type "record"
                                     :name "LongRecord"
                                     :fields  [{:name "value" :type "long"}]}}
              "string"
              :null
              {:type "array" :items [:null
                                     {:type "record"
                                      :name "LongList"
                                      :aliases ["LinkedLongs"]
                                      :fields  [{:name "value" :type "long"}
                                                {:name "next" :type ["null" "LongList"]}]}]}])

  ;;;
)
