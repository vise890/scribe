(ns scribe.avro.core-test
  (:require [abracad.avro :as aa]
            [abracad.avro.util :as au]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as sgen]
            [clojure.spec.test.alpha :as stest]
            [clojure.string :as string]
            [clojure.test :refer :all]
            [clojure.test.check.clojure-test :refer [defspec]]
            [clojure.test.check.properties :as prop]
            [medley.core :as m]
            [scribe.avro.core :as sut]
            [scribe.avro.schema-specs :as as]
            [scribe.decimal :as sut.decimal]
            [scribe.test-utils :as tu :refer [->decimal ->decimals]]
            [scribe.time.core :as t]))

(stest/instrument)

;;,-----------
;;| Test Utils
;;`-----------

(defn now-ms []
  (inst-ms (java.time.Instant/now)))

(defn valid-avro-schema?
  [as]
  (tu/is-valid ::as/avro-schema as)
  (is (aa/parse-schema as)))

(defn avro-schema-ok?
  ([spec-k]
   (testing "generated schema is s/valid :avro/schema"
     (valid-avro-schema? (sut/->avro-schema spec-k))))
  ([expected-schema spec-k]
   (let [got (sut/->avro-schema spec-k)]
     (valid-avro-schema? expected-schema)
     (testing "generated schema is s/valid :avro/schema"
       (valid-avro-schema? got))
     (testing "generated schema matches expectation"
       (is (= expected-schema got))))))

(defn roundtrips-to?
  [value-to-serialize expected-value-back spec-k]
  (s/assert spec-k value-to-serialize)
  (let [value-back (->> value-to-serialize
                        (sut/binary-encoded spec-k)
                        (sut/decode spec-k))]
    (is (= expected-value-back value-back))))

(defn can-roundtrip-ok?
  [value-to-serialize spec-k]
  (roundtrips-to? value-to-serialize value-to-serialize spec-k))

;;,----------------
;;| Primitive types
;;`----------------

;; https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html
;; https://avro.apache.org/docs/current/spec.html#schema_primitive

(s/def :s.primitive.preds/nil
  nil?)
(s/def :s.primitive.preds/boolean
  boolean?)
(s/def :s.primitive.preds/int
  int?)
(s/def :s.primitive.preds/posInt
  pos-int?)
(s/def :s.primitive.preds/negInt
  neg-int?)
(s/def :s.primitive.preds/natInt
  nat-int?)
(s/def :s.primitive.preds/float
  float?)
(s/def :s.primitive.preds/double
  double?)
(s/def :s.primitive.preds/string
  string?)
(deftest primitive-type-predicates-test
  (testing "nil?"
    (let [spec-k :s.primitive.preds/nil]
      (avro-schema-ok? {:type :null} spec-k)
      (can-roundtrip-ok? nil spec-k)))
  (testing "boolean?"
    (let [spec-k :s.primitive.preds/boolean]
      (avro-schema-ok? {:type :boolean} spec-k)
      (can-roundtrip-ok? true spec-k)
      (can-roundtrip-ok? false spec-k)
      (testing "can sneakily turn encode nil as boolean"
        (s/check-asserts false)
        (is (= false
               (->> nil
                    (sut/binary-encoded spec-k)
                    (sut/decode spec-k))))
        (s/check-asserts true))))
  (testing "int?"
    (let [spec-k :s.primitive.preds/int]
      (avro-schema-ok? {:type :long} spec-k)
      (can-roundtrip-ok? (int 42) spec-k)
      (can-roundtrip-ok? (byte 42) spec-k)
      (can-roundtrip-ok? 42 spec-k)))
  (testing "pos-int?"
    (let [spec-k :s.primitive.preds/posInt]
      (avro-schema-ok? {:type :long} spec-k)
      (can-roundtrip-ok? (int 42) spec-k)))
  (testing "neg-int?"
    (let [spec-k :s.primitive.preds/negInt]
      (avro-schema-ok? {:type :long} spec-k)
      (can-roundtrip-ok? (int -42) spec-k)))
  (testing "nat-int?"
    (let [spec-k :s.primitive.preds/natInt]
      (avro-schema-ok? {:type :long} spec-k)
      (can-roundtrip-ok? (int 42) spec-k)))
  (testing "float?"
    (let [spec-k :s.primitive.preds/float]
      (avro-schema-ok? {:type :double} spec-k)
      (can-roundtrip-ok? (float 42.0) spec-k)
      (can-roundtrip-ok? 42.0 spec-k)))
  (testing "double?"
    (let [spec-k :s.primitive.preds/double]
      (avro-schema-ok? {:type :double} spec-k)
      (can-roundtrip-ok? 42.0 spec-k)))
  (testing "string?"
    (let [spec-k :s.primitive.preds/string]
      (avro-schema-ok? {:type :string} spec-k)
      (can-roundtrip-ok? "foo" spec-k)))
  (testing "trying to serialize invalid val asserts via s/assert"
    (s/check-asserts true)
    (is (thrown? clojure.lang.ExceptionInfo
                 (sut/pre-serialize :s.primitive.preds/boolean "true")))))

(s/def :s.primitive.fns/boolean
  boolean)
(s/def :s.primitive.fns/short
  short)
(s/def :s.primitive.fns/int
  int)
(s/def :s.primitive.fns/long
  long)
(s/def :s.primitive.fns/double
  double)
(s/def :s.primitive.fns/int
  int)
(s/def :s.primitive.fns/float
  float)
(s/def :s.primitive.fns/double
  double)
(s/def :s.primitive.fns/str
  str)
(deftest primitive-type-fns-test
  (testing "boolean"
    (let [spec-k :s.primitive.fns/boolean]
      (avro-schema-ok? {:type :boolean} spec-k)
      (roundtrips-to? true true spec-k)
      (roundtrips-to? "true" true spec-k)))
  (testing "short"
    (let [spec-k :s.primitive.fns/short]
      (avro-schema-ok? {:type :int} spec-k)
      (can-roundtrip-ok? 42 spec-k)
      (roundtrips-to? 42.2 42 spec-k)))
  (testing "int"
    (let [spec-k :s.primitive.fns/int]
      (avro-schema-ok? {:type :int} spec-k)
      (can-roundtrip-ok? 42 spec-k)
      (roundtrips-to? 42.2 42 spec-k)))
  (testing "long"
    (let [spec-k :s.primitive.fns/long]
      (avro-schema-ok? {:type :long} spec-k)
      (can-roundtrip-ok? 42 spec-k)
      (roundtrips-to? 42.2 42 spec-k)))
  (testing "float"
    (let [spec-k :s.primitive.fns/float]
      (avro-schema-ok? {:type :float} spec-k)
      (can-roundtrip-ok? 42.0 spec-k)
      (roundtrips-to? 42 42.0 spec-k)))
  (testing "double"
    (let [spec-k :s.primitive.fns/double]
      (avro-schema-ok? {:type :double} spec-k)
      (can-roundtrip-ok? 42.0 spec-k)
      (roundtrips-to? 42 42.0 spec-k)))
  (testing "str"
    (let [spec-k :s.primitive.fns/str]
      (avro-schema-ok? {:type :string} spec-k)
      (can-roundtrip-ok? "foo" spec-k)
      (roundtrips-to? :foo ":foo" spec-k)))
  (testing "trying to serialize invalid val blows up"
    (s/check-asserts true)
    (is (thrown? ClassCastException
                 (sut/pre-serialize :s.primitive.fns/int "foo")))))

(s/def :s/boolean boolean?)
(s/def :s/string string?)

;;,--------------
;;| Logical Types
;;`--------------

(s/def :s/decimal
  (s/and decimal?
         (sut.decimal/->decimal-conformer :precision 17
                                          :scale 16)))
(deftest decimal-test
  (let [spec-k :s/decimal]
    (avro-schema-ok? {:type        :bytes
                      :logicalType :decimal
                      :precision   17
                      :scale       16}
                     spec-k)
    (can-roundtrip-ok? (->decimal 17 16) spec-k)))

;;,----------
;;| Timestamp
;;`----------

(s/def :s.time/inst inst?)
(deftest timestamp-inst-test
  (let [spec-k :s.time/inst]
    (avro-schema-ok? {:type        :long
                      :logicalType :timestamp-millis}
                     spec-k)
    (testing "can roundtrip java.time.Instant usual suspects"
      (can-roundtrip-ok? (java.time.Instant/now) spec-k)
      (binding [t/*instant-class* java.util.Date]
        (can-roundtrip-ok? (java.util.Date/from (java.time.Instant/now)) spec-k))
      (binding [t/*instant-class* java.sql.Timestamp]
        (can-roundtrip-ok? (java.sql.Timestamp. (now-ms)) spec-k)))))

(s/def :s.time/instant t/instant?)
(deftest timestamp-test
  (let [spec-k :s.time/instant]
    (testing "java.time.Instant and millis are used for defaults"
      (avro-schema-ok? {:type        :long
                        :logicalType :timestamp-millis}
                       spec-k)
      (can-roundtrip-ok? (java.time.Instant/now) spec-k))
    (testing "using dynamic var to change concrete `t/instant?` impl"
      (binding [t/*instant-class* java.util.Date]
        (can-roundtrip-ok? (java.util.Date/from (java.time.Instant/now)) spec-k))
      (binding [t/*instant-class* java.sql.Timestamp]
        (can-roundtrip-ok? (java.sql.Timestamp. (now-ms)) spec-k))
      (binding [t/*instant-class* org.joda.time.Instant]
        (can-roundtrip-ok? (org.joda.time.Instant/now) spec-k))
      (binding [t/*instant-class* java.lang.Long]
        (can-roundtrip-ok? (now-ms) spec-k))
      (testing "DateTime is returned in UTC"
        (binding [t/*instant-class* org.joda.time.DateTime]
          (let [now (org.joda.time.DateTime/now)
                got (->> now
                         (sut/binary-encoded :s.time/instant)
                         (sut/decode :s.time/instant))
                exp (.withZone now (org.joda.time.DateTimeZone/UTC))]
            (is (= exp got)))))))
  (testing "TODO conformer for `:timestamp-micros`"))

;;,-----
;;| Date
;;`-----

(s/def :s.time/date t/local-date?)
(deftest date-test
  (let [spec-k :s.time/date]
    (avro-schema-ok? {:type       :int
                      :logicalType :date}
                     spec-k)
    (testing "java.time.LocalDate is used by default"
      (can-roundtrip-ok? (java.time.LocalDate/now) spec-k))
    (testing "using dynamic var to change concrete `t/local-date?` impl"
      (binding [t/*local-date-class* org.joda.time.LocalDate]
        (can-roundtrip-ok? (org.joda.time.LocalDate/now) spec-k))
      (binding [t/*local-date-class* java.lang.Integer]
        (can-roundtrip-ok? (int (.toEpochDay (java.time.LocalDate/now))) spec-k))
      (binding [t/*local-date-class* java.lang.Long]
        (can-roundtrip-ok? (.toEpochDay (java.time.LocalDate/now)) spec-k)))))

;;,-----
;;| Time
;;`-----

(s/def :s.time/time t/local-time?)
(deftest time-test
  (let [spec-k :s.time/time]
    (testing "millis are used by default"
      (avro-schema-ok? {:type        :int
                        :logicalType :time-millis}
                       spec-k))
    (testing "java.time.LocalTime used by default"
      (can-roundtrip-ok? (java.time.LocalTime/now) spec-k))
    (testing "using dynamic var to change concrete `t/local-time?` impl"
      (binding [t/*local-time-class* org.joda.time.LocalTime]
        (can-roundtrip-ok? (org.joda.time.LocalTime/now) spec-k))
      (binding [t/*local-time-class* java.lang.Integer]
        (can-roundtrip-ok? (int 42) spec-k))
      (binding [t/*local-time-class* java.lang.Long]
        (can-roundtrip-ok? 42 spec-k))))
  (testing "TODO conformer for `:time-micros`"))

;;,--------------
;;| Complex Types
;;`--------------

(s/def :s/enum
  #{:beep :boop})
(deftest enum-test
  (testing "non-empty, keyword-only sets are converted to Avro enums"
    (let [spec-k :s/enum]
      (avro-schema-ok? {:type      :enum
                        :name      "enum"
                        :namespace "s"
                        :symbols   #{:beep :boop}}
                       spec-k)
      (can-roundtrip-ok? :beep spec-k)
      (can-roundtrip-ok? :boop spec-k)
      (testing "serializing a string gets you back a keyword"
        (s/check-asserts false)
        (let [schema (sut/->avro-schema spec-k)]
          (is (= :beep
                 (->> "beep"
                      (sut/binary-encoded spec-k)
                      (sut/decode spec-k)))))
        (testing "but it will throw if check-asserts is set to true"
          (s/check-asserts true)
          (is (thrown? clojure.lang.ExceptionInfo
                       (sut/binary-encoded spec-k "beep"))))))))

(s/def :s/collOf
  (s/coll-of string?))
(s/def :s.collOf/specRef
  (s/coll-of :s/string))
(s/def :s.collOf/decimal
  (s/coll-of :s/decimal))
(deftest coll-of-test
  (testing "coll-of is converted to an Avro array"
    (avro-schema-ok? {:type      :array
                      :items     {:type :string}}
                     :s/collOf)
    (can-roundtrip-ok? ["foo"] :s/collOf)
    (can-roundtrip-ok? '("yo" "lo") :s/collOf))
  (testing "referencing other specs works"
    (let [spec-k :s.collOf/specRef]
      (avro-schema-ok? {:type      :array
                        :items     {:type :string}}
                       spec-k)
      (can-roundtrip-ok? '("yo" "lo") spec-k)))
  (testing "items type that needs pre-serialization/post-deserialization"
    (let [spec-k :s.collOf/decimal]
      (avro-schema-ok? {:type      :array
                        :items     {:type        :bytes
                                    :logicalType :decimal
                                    :precision   17
                                    :scale       16}}
                       spec-k)
      (can-roundtrip-ok? (take 3 (->decimals 17 16)) spec-k))))

(s/def :s/mapOf
  (s/map-of string? string?))
(s/def :s.mapOf/specRef
  (s/map-of string? :s/string))
(s/def :s.mapOf/decimal
  (s/map-of string? :s/decimal))
(s/def :s.mapOf/nonStringKeys
  (s/map-of int? :s/string))
(deftest map-of-test
  (testing "coll-of is converted to an Avro array"
    (avro-schema-ok? {:type   :map
                      :values {:type :string}}
                     :s/mapOf)
    (can-roundtrip-ok? {"hi" "you"} :s/mapOf))
  (testing "referencing other specs works"
    (let [spec-k :s.mapOf/specRef]
      (avro-schema-ok? {:type   :map
                        :values {:type :string}}
                       spec-k)
      (can-roundtrip-ok? {"hi" "you"} spec-k)))
  (testing "items type that needs pre-serialization/post-deserialization"
    (let [spec-k :s.mapOf/decimal]
      (avro-schema-ok? {:type   :map
                        :values {:type        :bytes
                                 :logicalType :decimal
                                 :precision   17
                                 :scale       16}}
                       spec-k)
      (can-roundtrip-ok? (sgen/generate (s/gen :s.mapOf/decimal))
                         spec-k)))
  (testing "you get back strings for keys, regardless of their input spec"
    ;; NOTE the Avro spec only supports string keys...
    ;; https://avro.apache.org/docs/current/spec.html#Maps
    (let [spec-k    :s.mapOf/nonStringKeys
          a-map-in  (sgen/generate (s/gen :s.mapOf/nonStringKeys))
          a-map-exp (m/map-keys str a-map-in)]
      (roundtrips-to? a-map-in
                      a-map-exp
                      spec-k))))

(s/def :s/keys
  (s/keys :req-un [:s/string]))
(s/def :s.keys/optUn
  (s/keys :req-un [:s/string] :opt-un [:s/boolean]))
;; TODO deal with kebab-case same way as abracad
(s/def :s.keys/withPrepostSerde
  (s/keys :req-un [:s/string :s/decimal]))
(deftest keys-test
  (testing "keys is converted to an Avro record"
    (let [spec-k :s/keys]
      (avro-schema-ok? {:type      :record
                        :name      "keys"
                        :namespace "s"
                        :fields    [{:name "string"
                                     :type {:type :string}}]}
                       spec-k)
      (can-roundtrip-ok? {:string "baz"} spec-k)))
  (testing "opt-un keys are marked as optional"
    (let [spec-k :s.keys/optUn]
      (avro-schema-ok? {:type      :record
                        :name      "optUn"
                        :namespace "s.keys"
                        :fields    [{:name "string"
                                     :type {:type :string}}
                                    {:name "boolean"
                                     :default nil
                                     :type [:null {:type :boolean}]}]}
                       spec-k)
      (can-roundtrip-ok? {:string "baz"} spec-k)
      (can-roundtrip-ok? {:string "baz" :boolean true} spec-k)))
  (testing "a field value that requires pre-serialization/post-deserialization"
    (let [spec-k :s.keys/withPrepostSerde]
      (avro-schema-ok? {:type      :record
                        :name      "withPrepostSerde"
                        :namespace "s.keys"
                        :fields    [{:name "string"
                                     :type {:type :string}}
                                    {:name "decimal"
                                     :type {:type        :bytes
                                            :logicalType :decimal
                                            :precision   17
                                            :scale       16}}]}
                       spec-k)
      (can-roundtrip-ok? {:string "foo" :decimal (->decimal 17 16)} spec-k))))

(s/def :s/or
  (s/or :string :s/string
        :boolean :s/boolean))
(deftest or-test
  (testing "s/or is converted to an Avro union"
    (let [spec-k :s/or]
      (avro-schema-ok? [{:type :string}
                        {:type :boolean}]
                       spec-k)
      (can-roundtrip-ok? "hi" spec-k)
      (can-roundtrip-ok? true spec-k))))

;; TODO inline specs in s/or
;; (s/def :s.or/nested-keys
;;   (s/or :r1 (s/keys :req-un [:s/keys :s/string])
;;         :r2 (s/keys :req-un [:s/keys :s/boolean])))
(s/def :s/record1
  (s/keys :req-un [:s/keys :s/string]))
(s/def :s/record2
  (s/keys :req-un [:s/keys :s/boolean]))
(s/def :s.or/nestedKeys
  (s/or :r1 :s/record1
        :r2 :s/record2))
(s/def :s.or/inlineSpecs
  (s/or :1 (s/keys :req-un [:s/string])
        :2 :s/string))
(deftest or-avro-ref-test
  (testing "records are defined on first appearance only, after which they're referred to"
    (let [spec-k     :s.or/nestedKeys
          exp-schema [{:name      "record1"
                       :namespace "s"
                       :type      :record
                       :fields
                       [{:name "keys"
                         :type {:name      "keys"
                                :namespace "s"
                                :type      :record
                                :fields
                                [{:name "string",
                                  :type {:type :string}}]}}
                        {:name "string"
                         :type {:type :string}}]}
                      {:name      "record2"
                       :namespace "s"
                       :type      :record
                       :fields
                       [{:name "keys",
                         ;; NOTE :type = ns+name
                         :type "s.keys"}
                        {:name "boolean"
                         :type {:type :boolean}}]}]]
      (avro-schema-ok? exp-schema spec-k)
      (can-roundtrip-ok? {:keys   {:string "foo"}
                          :string "bar"}
                         spec-k)
      (can-roundtrip-ok? {:keys    {:string "beep"}
                          :boolean false}
                         spec-k)))
  ;; FIXME I FAIL
  #_(testing "inline specs"
      (let [spec-k :s.or/inlineSpecs]
        (valid-avro-schema? spec-k)
        (can-roundtrip "foo" spec-k)
        (can-roundtrip {:string "foo"} spec-k))))

(s/def :s.or.serde/recordOne
  (s/keys :req-un [:s/decimal :s/string]))
(s/def :s.or.serde/recordTwo
  (s/keys :req-un [:s/decimal :s/boolean]))
(s/def :s.or.serde/or
  (s/or :r1 :s.or.serde/recordOne
        :r2 :s.or.serde/recordTwo))
(deftest or-serde-test
  (let [recordOne-spec-k :s.or.serde/recordOne
        recordTwo-spec-k :s.or.serde/recordTwo
        recordOne-exp-schema
        {:type      :record
         :name      "recordOne"
         :namespace "s.or.serde"
         :fields    [{:name "decimal"
                      :type {:logicalType :decimal
                             :type        :bytes
                             :precision   17
                             :scale       16}}
                     {:name "string"
                      :type {:type :string}}]}
        recordTwo-exp-schema
        {:type      :record
         :name      "recordTwo"
         :namespace "s.or.serde"
         :fields    [{:name "decimal"
                      :type {:logicalType :decimal
                             :type        :bytes
                             :precision   17
                             :scale       16}}
                     {:name "boolean"
                      :type {:type :boolean}}]}
        union-spec-k     :s.or.serde/or
        union-exp-schema    [recordOne-exp-schema recordTwo-exp-schema]]
    (testing "recordOne"
      (testing "schema is ok"
        (avro-schema-ok? recordOne-exp-schema recordOne-spec-k))
      (testing "can be roundtripped"
        (can-roundtrip-ok? (sgen/generate (s/gen recordOne-spec-k))
                           recordOne-spec-k)))
    (testing "recordTwo"
      (testing "schema is ok"
        (avro-schema-ok? recordTwo-exp-schema recordTwo-spec-k))
      (testing "can be roundtripped"
        (can-roundtrip-ok? (sgen/generate (s/gen recordTwo-spec-k))
                           recordTwo-spec-k)))
    (testing "union"
      (testing "schema is ok"
        (avro-schema-ok? union-exp-schema union-spec-k))
      (testing ", the member recordOne can be roundtripped"
        (can-roundtrip-ok? (sgen/generate (s/gen recordOne-spec-k))
                           union-spec-k))
      (testing ", the member recordTwo can be roundtripped"
        (can-roundtrip-ok? (sgen/generate (s/gen recordTwo-spec-k))
                           union-spec-k)))))

(s/def :s.and/nonBlankString
  (s/and string? (complement string/blank?)))
(s/def :s.and/posDecimal
  (s/with-gen
    (s/and decimal?
           pos?
           (sut.decimal/->decimal-conformer :precision 4
                                            :scale 4))
    (fn []
      (sgen/fmap (fn [^BigDecimal bd] (.abs bd))
                 (s/gen decimal?)))))
(s/def :s.and/empty
  (s/and string? (s/and)))
(s/def :s.and/unknownPred
  (s/and string? (fn [x] (and (any? x) (some? x)))))
(s/def :s.and/string+junk
  (s/and any? some? string? (complement string/blank?)))
(s/def :s.and.avro-ref/inner-record
  (s/keys :req-un [:s/string]))
(s/def :s.and.avro-ref/field1 (s/and :s.and.avro-ref/inner-record))
(s/def :s.and.avro-ref/field2 (s/and :s.and.avro-ref/inner-record))
(s/def :s.and.avro-ref/outer-record
  (s/keys :req-un [:s.and.avro-ref/field1
                   :s.and.avro-ref/field2]))
(s/def :s.and/innerNamedMember
  (s/and keyword? #{:foo :bar}))
(deftest and-test
  (testing "string"
    (let [spec-k :s.and/nonBlankString]
      (avro-schema-ok? {:type :string} spec-k)
      (can-roundtrip-ok? "foo" spec-k)))

  (testing "values that require serde"
    (let [spec-k :s.and/posDecimal]
      (avro-schema-ok? {:type        :bytes
                        :logicalType :decimal
                        :precision   4
                        :scale       4}
                       spec-k)
      (can-roundtrip-ok? (->> 4
                              (->decimals 4)
                              (filter pos?)
                              first)
                         spec-k)))

  (testing "empty s/and doesn't blow up"
    (let [spec-k :s.and/empty]
      (avro-schema-ok? {:type :string} spec-k)
      (can-roundtrip-ok? "foo" spec-k)))

  (testing "predicates that are not recognised are ignored"
    (let [spec-k :s.and/unknownPred]
      (avro-schema-ok? {:type :string} spec-k)
      (can-roundtrip-ok? "foo" spec-k)))

  (testing "as long as something can be used, it will be"
    (let [spec-k :s.and/string+junk]
      (avro-schema-ok? {:type :string} spec-k)
      (can-roundtrip-ok? "bar" spec-k)))

  (testing "merging avro-refs inside s/and"
    (let [spec-k :s.and.avro-ref/outer-record]
      (avro-schema-ok? {:name "outer_record"
                        :namespace "s.and.avro_ref"
                        :type :record
                        :fields [{:name "field1"
                                  :type {:name "inner_record"
                                         :namespace "s.and.avro_ref"
                                         :type :record
                                         :fields [{:name "string"
                                                   :type {:type :string}}]}}
                                 {:name "field2"
                                  :type "s.and.avro_ref.inner_record"}]}
                       spec-k)
      (can-roundtrip-ok? {:field1 {:string "foo"}
                          :field2 {:string "bar"}}
                         spec-k)))

  (testing "inner named members (e.g. enums) work to a certain extent"
    (let [spec-k :s.and/innerNamedMember]
      (avro-schema-ok? {:name "innerNamedMember",
                        :namespace "s.and",
                        :symbols #{:bar :foo},
                        :type :enum}
                       spec-k)
      (can-roundtrip-ok? :foo spec-k))))

(s/def :s/merge
  (s/merge (s/keys :req-un [:s/boolean]
                   :opt-un [:s/string])
           (s/keys :req-un [:s.and/nonBlankString])))
(s/def :s.merge/serde
  (s/merge :s/merge
           (s/keys :req-un [:s/decimal])))
(deftest merge-test
  (testing "can roundtrip"
    (let [spec-k :s/merge
          m      (sgen/generate (s/gen spec-k))
          exp-schema
          {:name      "merge",
           :namespace "s",
           :type      :record
           :fields    [{:name "boolean",
                        :type {:type :boolean}}
                       {:name "string",
                        :default nil
                        :type [:null {:type :string}]}
                       {:name "nonBlankString", :type {:type :string}}]}]
      (avro-schema-ok? exp-schema spec-k)
      (can-roundtrip-ok? m spec-k)))
  (testing "serde"
    (let [spec-k :s.merge/serde
          m      (sgen/generate (s/gen spec-k))
          exp-schema
          {:name      "serde",
           :namespace "s.merge",
           :type      :record
           :fields    [{:name "boolean",
                        :type {:type :boolean}}
                       {:name "string",
                        :default nil
                        :type [:null {:type :string}]}
                       {:name "nonBlankString", :type {:type :string}}
                       {:type {:type        :bytes,
                               :logicalType :decimal,
                               :precision   17,
                               :scale       16},
                        :name "decimal"}]}]
      (avro-schema-ok? exp-schema spec-k)
      (can-roundtrip-ok? m spec-k))))

(s/def :s.mangle-names/my-field string?)
(s/def :s.mangle-names/my-record
  (s/keys :req-un [:s.mangle-names/my-field]))
(s/def :s.mangle-names/another-record
  (s/keys :req-un [:s.mangle-names/my-record]))
(s/def :s.mangle-names/my-union
  (s/or :r :s.mangle-names/my-record
        :ar :s.mangle-names/another-record))
(deftest mangled-names-test
  (testing "hyphens are replaced by underscores"
    (avro-schema-ok? [{:name      "my_record",
                       :namespace "s.mangle_names",
                       :fields    [{:name "my_field",
                                    :type {:type :string}}],
                       :type      :record}
                      {:name      "another_record",
                       :namespace "s.mangle_names",
                       :fields    [{:name "my_record",
                                    :type "s.mangle_names.my_record"}],
                       :type      :record}]
                     :s.mangle-names/my-union)))
(defspec record-field-name-mangling-test
  10
  (prop/for-all [r (s/gen :s.mangle-names/my-record)]
                (= r
                   (->> r
                        (sut/binary-encoded :s.mangle-names/my-record)
                        (sut/decode :s.mangle-names/my-record)))))
(defspec avro-ref-name-mangling-test
  10
  (prop/for-all [r (s/gen :s.mangle-names/my-union)]
                (= r
                   (->> r
                        (sut/binary-encoded :s.mangle-names/my-union)
                        (sut/decode :s.mangle-names/my-union)))))
(deftest disabled-name-mangling-test
  (testing "can turn off with `au/*mangle-names*`"
    (binding [au/*mangle-names* false]
      (is (thrown? Exception
                   (sut/binary-encoded :s.mangle-names/my-record
                                       (sgen/generate
                                        (s/gen :s.mangle-names/my-record))))))))

;;,------------
;;| STRESS TEST
;;`------------

(s/def :s.stressTest/r1Field
  string?)
(s/def :s.stressTest/r2Field
  string?)

(s/def :s.stressTest/recordOne
  (s/keys :req-un [:s.stressTest/r1Field
                   :s/enum
                   :s/keys
                   :s/merge]
          :opt-un [:s/string
                   :s/collOf
                   :s.collOf/decimal]))
(s/def :s.stressTest/recordTwo
  (s/merge (s/keys :req-un [:s.stressTest/r2Field
                            :s.keys/withPrepostSerde
                            :s.or/nestedKeys
                            :s.and/posDecimal
                            :s.stressTest/recordOne]
                   :opt-un [:s/string
                            :s/boolean])
           (s/keys :req-un [:s/enum]
                   :opt-un [:s.keys/optUn
                            :s.collOf/decimal])))
(s/def :s.stressTest/or
  (s/or :r1 :s.stressTest/recordOne
        :r2 :s.stressTest/recordTwo))

(deftest stress-avro-schema-test
  (valid-avro-schema? (sut/->avro-schema :s.stressTest/recordOne))
  (valid-avro-schema? (sut/->avro-schema :s.stressTest/recordTwo))
  (valid-avro-schema? (sut/->avro-schema :s.stressTest/or)))

(defspec stress-test
  10
  (prop/for-all [r (s/gen :s.stressTest/or)]
                (= r
                   (->> r
                        (sut/binary-encoded :s.stressTest/or)
                        (sut/decode :s.stressTest/or)))))
