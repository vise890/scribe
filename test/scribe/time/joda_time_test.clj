(ns scribe.time.joda-time-test
  (:require [clojure.test :refer :all]
            [scribe.time.core :as t])
  (:import [org.joda.time DateTime Instant LocalDate LocalTime]))

(deftest preds-test

  (is (t/instant? (Instant/now)))
  (is (t/instant? (DateTime/now)))
  (is (t/local-date? (LocalDate/now)))
  (is (t/local-time? (LocalTime/now)))

  (is (not (t/local-date? (Instant/now))))
  (is (not (t/local-time? (Instant/now))))

  (is (not (t/local-date? (DateTime/now))))
  (is (not (t/local-time? (DateTime/now))))

  (is (not (t/instant? (LocalDate/now))))
  (is (not (t/local-time? (LocalDate/now))))

  (is (not (t/instant? (LocalTime/now))))
  (is (not (t/local-date? (LocalTime/now)))))

(defn inst-roundtrip-tests [inst]
  (let [roundtrip-fn (fn [i] (->> i
                                  (t/to-epoch-milli)
                                  (t/of-epoch-milli (.getClass inst))))]
    (testing "round-tripping an instant"
      (let [rt (roundtrip-fn inst)]
        (testing "yields the original value"
          (is (= inst rt)))))))

(deftest Instant<->epoch-millis
  (inst-roundtrip-tests (Instant/parse "2007-12-03T10:15:30.000Z")))

(deftest DateTime<->epoch-millis
  (inst-roundtrip-tests (DateTime/parse "2007-12-03T10:15:30.000Z")))

(deftest LocalDate<->epoch-days
  ;; TODO FAIL implement
  #_(let [ld (LocalDate/parse "2007-12-03")]
      (is (= ld
             (->> ld
                  t/to-epoch-days
                  (t/of-epoch-days LocalDate))))))

(deftest LocalTime<->millis-of-day
  (let [ld (LocalTime/parse "T10:15:30.000")]
    (is (= ld
           (->> ld
                t/to-milli-of-day
                (t/of-milli-of-day LocalTime))))))
