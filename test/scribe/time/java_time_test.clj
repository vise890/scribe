(ns scribe.time.java-time-test
  (:require [clojure.test :refer :all]
            [scribe.time.core :as t])
  (:import [java.time Instant LocalDate LocalTime]))

(deftest preds-test
  (is (t/instant? (Instant/now)))
  (is (t/local-date? (LocalDate/now)))
  (is (t/local-time? (LocalTime/now)))

  (is (not (t/local-date? (Instant/now))))
  (is (not (t/local-time? (Instant/now))))

  (is (not (t/instant? (LocalDate/now))))
  (is (not (t/local-time? (LocalDate/now))))

  (is (not (t/instant? (LocalTime/now))))
  (is (not (t/local-date? (LocalTime/now)))))

(deftest sanity-checks
  (testing "toEpochMilli truncates nanos"
    (is (= (.toEpochMilli Instant/EPOCH)
           (-> Instant/EPOCH
               (.plusNanos 999999)
               .toEpochMilli)))))

(defn inst-roundtrip-tests [roundtrip-fn]
  (let [half-ms-in-ns (int (/ 1e6 2)) ;; i.e. = 500,000ns
        inst (Instant/parse "2007-12-03T10:15:30.000Z")]
    (testing "round-tripping an instant with nanos = 0"
      (let [rt (roundtrip-fn inst)]
        (testing "yields the original value"
          (is (= inst rt)))))
    (testing "round-tripping an instant with nanos = 499,999"
      (let [inst-at-last-nano-of-this-milli ;; i.e. inst + 499,999ns
            (-> inst (.plusNanos (dec half-ms-in-ns)))
            rt (roundtrip-fn inst-at-last-nano-of-this-milli)]
        (assert (= (.getNano inst-at-last-nano-of-this-milli) 499999))
        (testing "yields the same instant"
          (is (= inst rt)))))
    (testing "round-tripping an instant with nanos = 499,999"
      (let [inst-at-first-nano-of-next-milli ;; i.e. inst + 500,001ns
            (-> inst (.plusNanos (inc half-ms-in-ns)))
            rt (roundtrip-fn inst-at-first-nano-of-next-milli)]
        (assert (= (.getNano inst-at-first-nano-of-next-milli) 500001))
        (testing "truncates the nanos and yields the original instant"
          (is (= inst rt)))))))

(deftest Instant<->epoch-millis
  (inst-roundtrip-tests (fn [i] (->> i
                                     (t/to-epoch-milli)
                                     (t/of-epoch-milli Instant)))))

(deftest Instant<->epoch-micros
  ;; TODO FAIL implement
  #_(roundtrip-tests (fn [i] (->> i
                                  (t/to-epoch-micros)
                                  (t/of-epoch-micros Instant)))))

(deftest LocalDate<->epoch-days
  (let [ld (LocalDate/parse "2007-12-03")]
    (is (= ld
           (->> ld
                t/to-epoch-day
                (t/of-epoch-day LocalDate))))))

(deftest LocalTime<->millis-of-day
  ;; TODO FAIL implement
  #_(let [ld (LocalTime/parse "10:15:30.000")]
      (is (= ld
             (->> ld
                  t/to-millis-of-day
                  (t/of-millis-of-day LocalTime))))))
