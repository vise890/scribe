(ns scribe.test-utils
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as sgen]
            [clojure.test :as t]))

(defmacro is-valid
  [spec value]
  `(t/is (s/valid? ~spec ~value) (s/explain-str ~spec ~value)))

(defmacro is-not-valid [spec value] `(t/is (not (s/valid? ~spec ~value))))

(defn ->decimals
  ([] (repeatedly (fn [] (sgen/generate (s/gen decimal?)))))
  ([p s]
   (->> (->decimals)
        (filter #(and (= p (.precision %)) (= s (.scale %)))))))

(defn ->decimal [p s] (first (->decimals p s)))

;;,--------------------------------------
;;| Finding vals spec can generate easily
;;`--------------------------------------

(defn modes
  [coll]
  (->> coll
       frequencies
       (sort-by val)))

(defn top3
  [coll]
  (->> coll
       modes
       reverse
       (take 3)))

(defn ->precisions
  []
  (->> (->decimals)
       (map #(.precision %))))

(defn ->scales
  []
  (->> (->decimals)
       (map #(.precision %))))

(comment ;;;
  (top3 (take 1000 (->precisions)))
  (top3 (take 1000 (->scales)))
         ;;;
)
