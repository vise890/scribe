(ns scribe.json-schema.specs
  "Clojure Spec definitions for JSON Schemas. http://json-schema.org"
  (:require [clojure.spec.alpha :as s]))

(s/def :JSONSchema/type string?)
(defmacro ^:private type=
  [t]
  `(s/and (s/keys :req-un [:JSONSchema/type])
          (fn [x#] (= ~(name t) (:type x#)))))

(s/def :JSONSchema/string
  (type= :string))
(s/def :JSONSchema/boolean
  (type= :boolean))

(s/def :JSONSchema/format
  string?)
(defmacro ^:private format=
  [f]
  `(s/and (s/keys :req-un [:JSONSchema/format])
          (fn [x#] (= ~(name f) (:format x#)))))

(s/def :JSONSchema.integer/int32
  (format= :int32))
(s/def :JSONSchema.integer/int64
  (format= :int64))
(s/def :JSONSchema/integer
  (s/and (type= :integer)
         (s/or :JSONSchema.integer/int32 :JSONSchema.integer/int32
               :JSONSchema.integer/int64 :JSONSchema.integer/int64)))

(s/def :JSONSchema.number/double
  (format= :double))
(s/def :JSONSchema/number
  (s/and (type= :number)
         (s/or :JSONSchema.number/double :JSONSchema.number/double)))

(s/def :JSONSchema.Prop.array/items
  ;; HACK for yet undefined spec
  (s/and :JSONSchema/Schema))
(s/def :JSONSchema/array
  (s/and (type= :array)
         (s/keys :req-un [:JSONSchema/type
                          :JSONSchema.prop.array/items])))

(s/def :JSONSchema.$ref/$ref
  string?) ;; TODO needs to be valid
(s/def :JSONSchema/$ref
  ;; HACK for yet undefined spec
  (s/and (s/keys :req-un [:JSONSchema.$ref/$ref])))

(s/def :JSONSchema.object/property-name
  (s/and (s/or :str string?, :kw keyword?)
         (s/conformer keyword)))
(s/def :JSONSchema.object/properties
  (s/or :properties
        (s/map-of :JSONSchema.object/property-name :JSONSchema/Schema)
        :JSONSchema/Schema (s/and :JSONSchema/Schema)))
(s/def :JSONSchema.object/additionalProperties
  :JSONSchema.object/properties)
(s/def :JSONSchema.object/required
  (s/coll-of :JSONSchema.object/property-name))
(s/def :JSONSchema/object
  (s/and (s/keys :opt-un [:JSONSchema/type
                          :JSONSchema.object/required
                          :JSONSchema.object/properties
                          :JSONSchema.object/additionalProperties]
                 :req-un [])
         (fn [x] (let [t (:type x)]
                   (or (nil? t)
                       (= "object" t))))))

(s/def :JSONSchema/description
  string?)
(s/def :JSONSchema/Schema
  (s/and (s/keys :opt-un [:JSONSchema/description])
         (s/or :JSONSchema/string :JSONSchema/string
               :JSONSchema/boolean :JSONSchema/boolean
               :JSONSchema/integer :JSONSchema/integer
               :JSONSchema/number :JSONSchema/number
               :JSONSchema/array :JSONSchema/array
               :JSONSchema/object :JSONSchema/object
               :JSONSchema/$ref :JSONSchema/$ref)))
