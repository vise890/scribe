(ns scribe.avro.schema-specs
  "Validations for Avro schemas according to
  https://avro.apache.org/docs/current/spec.html"
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as string]))

(defn- type= [t]
  (fn [x]
    (= (keyword t)
       (keyword (-> x :type)))))

(defn- name-conformer
  [x]
  (try
    (name x)
    (catch Exception _
      :clojure.spec.alpha/invalid)))

;;,------------------------------------------------------------
;;| Names: https://avro.apache.org/docs/current/spec.html#names
;;`------------------------------------------------------------
(defn valid-name?
  "Returns whether or not a given string is a valid Avro name.

  See: https://avro.apache.org/docs/current/spec.html#names"
  [s]
  (boolean (re-matches #"^[A-Za-z_][A-Za-z0-9_]*$" s)))

(defn valid-namespace?
  "Returns whether or not a given string is a valid Avro namespace.

   See: https://avro.apache.org/docs/current/spec.html#names"
  [x]
  (every? valid-name? (string/split x #"\.")))

(s/def :avro/name
  (s/and (s/conformer name-conformer)
         valid-name?))
(s/def :avro/namespace
  (s/and (s/conformer name-conformer)
         valid-namespace?))
(s/def :avro/qualified-name :avro/namespace)

(s/def :avro.name.wrapped/type :avro/name) ;; oh ffs..
(s/def :avro.name/wrapped
  (s/and (s/keys :req-un [:avro.name.wrapped/type])
         (comp not (type= :record))))

;;,----------------------------------------------------------------
;;| Primitive Types:
;;| https://avro.apache.org/docs/current/spec.html#schema_primitive
;;`----------------------------------------------------------------
(defn primitive-type?
  "Returns whether or not a given string/keyword is an Avro Primitive Type.

   See: https://avro.apache.org/docs/current/spec.html#schema_primitive"
  [x]
  (#{:null
     :int :long :float :double
     :bytes :boolean :string} (keyword x)))
(s/def :avro/primitive
  (s/and :avro/name primitive-type?))

;;,---------------
;;| Logical Types:
;;| https://avro.apache.org/docs/current/spec.html#Logical+Types
;;`---------------
(defn logical-type?
  "Returns whether or not a given string/keyword is an Avro Logical Type.

   See: https://avro.apache.org/docs/current/spec.html#Logical+Types"
  [x]
  (#{:decimal
     :date
     :time-millis :time-micros
     :timestamp-millis :timestamp-micros}  (keyword x)))

;;,----------------------------------------------------------------------
;;| Records: https://avro.apache.org/docs/current/spec.html#schema_record
;;`----------------------------------------------------------------------
(s/def :avro.record.field/type
  ;; NOTE s/and : hack for yet undefined spec
  (s/and :avro/schema))
(s/def :avro.record/field
  (s/and (s/keys :req-un [:avro/name
                          :avro.record.field/type]
                 :opt-un [::namespace])))
(s/def :avro.record/fields
  (s/and (s/coll-of :avro.record/field)
         seq))
(s/def :avro/record
  (s/and (type= :record)
         (s/keys :req-un [:avro/name
                          :avro.record/fields]
                 :opt-un [:avro/namespace])))

;;,------------------------------------------------------------
;;| Enums: https://avro.apache.org/docs/current/spec.html#Enums
;;`------------------------------------------------------------
(s/def :avro.enum/symbol
  (s/and (s/or :s string? :k keyword?)
         (s/conformer (comp keyword second))))
(s/def :avro.enum/symbols
  (s/coll-of :avro.enum/symbol))
(s/def :avro/enum
  (s/and (type= :enum)
         (s/keys :req-un [:avro/name
                          :avro.enum/symbols]
                 :opt-un [:avro/namespace])))

;;,--------------------------------------------------------------
;;| Arrays: https://avro.apache.org/docs/current/spec.html#Arrays
;;`--------------------------------------------------------------
(s/def :avro.array/items
  ;; NOTE s/and : hack for yet undefined spec
  (s/and :avro/schema))
(s/def :avro/array
  (s/and (type= :array)
         (s/keys :req-un [:avro.array/items])))

;;,--------------------------------------------------------------
;;| Maps: https://avro.apache.org/docs/current/spec.html#Maps
;;`--------------------------------------------------------------
(s/def :avro.map/values
  ;; NOTE s/and : hack for yet undefined spec
  (s/and :avro/schema))
(s/def :avro/map
  (s/and (type= :map)
         (s/keys :req-un [:avro.map/values])))

;;,--------------------------------------------------------------
;;| Unions: https://avro.apache.org/docs/current/spec.html#Unions
;;`--------------------------------------------------------------
(s/def :avro/union
  (s/and sequential?
         (s/coll-of :avro/schema)
         seq))

;; TODO plug in abracad/parse-schema for final sanity check

;;,-----------------------------------------------------------------
;;| The Avro Schema in all its Glory. Praise be to the Lord of Types
;;`-----------------------------------------------------------------
(s/def :avro/schema
  ;; NOTE order is important here!
  (s/or :primitive :avro/primitive
        ;; the `type-name` here refers to a derived type name (since
        ;; :avro/primitive would have already been matched). A Record name, for
        ;; example. https://avro.apache.org/docs/current/spec.html#schemas
        :type-name :avro/name
        :qualified-type-name :avro/qualified-name
        :record :avro/record
        :enum :avro/enum
        :array :avro/array
        :map :avro/map
        :union :avro/union
        ;; TODO fixed
        :gratuitious-map-type-wrapper :avro.name/wrapped))
(s/def ::avro-schema :avro/schema)
