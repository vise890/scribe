(ns scribe.avro.sanitation-utils
  "Oh boy.
  Basically Avro is quite strict in what characters are allowed in names.
  These are utilities to deal with that."
  (:require [camel-snake-kebab.core :as csk]
            [clojure.string :as string]
            [clojure.walk :as walk]
            [scribe.avro.schema-specs :as as]))

(defn sanitize-name
  [n]
  ;; https://avro.apache.org/docs/current/spec.html#names
  (if (as/valid-namespace? n) n (str "_" n)))

(defn desanitize-name
  [n]
  (if ((fnil string/starts-with? "") n "_") (subs n 1) n))

(def sanitize-keyword (comp keyword sanitize-name name))
(def desanitize-keyword (comp keyword desanitize-name name))

(defn ->name
  [n]
  (->> n
       name
       csk/->camelCase
       sanitize-name))
(defn ->Name
  [n]
  (->> n
       ->name
       csk/->PascalCase
       sanitize-name))
(defn ->ns
  [& chunks]
  (->> chunks
       (map name)
       (mapcat #(string/split % #"\."))
       (map ->name)
       (string/join ".")))

(defn ->xform-keyword-keys-fn
  [xform]
  (fn [m]
    (let [f (fn [[k v]] (if (keyword? k) [(xform k) v] [k v]))]
      ;; only apply to maps
      (walk/postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) m))))

(def
  ^{:doc
    "Recursively sanitizes the keys of a map to conform with the Avro spec",
    :private true}
  sanitize-keys
  (->xform-keyword-keys-fn sanitize-keyword))

(def ^{:doc "Recursively de-sanitizes the keys of a map", :private true}
  desanitize-keys
  (->xform-keyword-keys-fn desanitize-keyword))

(defn de-avro-ise [edn] (desanitize-keys edn))

(defn avro-ise [edn] (sanitize-keys edn))
