(ns scribe.avro.core
  "Functions for Clojure Spec to Avro schema conversion, encoding and decoding."
  (:require [abracad.avro :as aa]
            [abracad.avro.util :as au]
            [clojure.spec.alpha :as s]
            [clojure.tools.logging :as log]
            [medley.core :as m]
            [scribe.decimal :as decimal]
            [scribe.time.core :as t]
            [scribe.utils :refer :all]))

(s/def ::enum
  (s/and set? not-empty (partial every? keyword?)))
(defn enum?
  "Specs defined as non-empty, keyword-only sets are `::enum`s.

  `::enum`s are used for defining a finite set of choices and are converted to
  Avro `:enum`s. An example would be:

  `(s/def ::color #{:red :green :blue})'"
  [x]
  (s/valid? ::enum x))
(s/def ::spec-k qualified-keyword?)
(s/def ::spec
  (s/or :spec-k ::spec-k
        :pred symbol?
        :form seq?
        :enum ::enum))
(defn- spec-variant
  [spec]
  (first (s/conform ::spec spec)))

(s/fdef spec-dispatch
  :args (s/cat :spec ::spec))
(defn- spec-dispatch
  [spec]
  (case (spec-variant spec)
    :spec-k ::spec-k
    :pred   spec
    :enum   ::enum
    :form   (first spec)))

(def ^:private ^:dynamic *spec-k* nil)

(defn- or-preds
  [or-spec-form]
  (->> (rest or-spec-form) (partition 2) (map second)))

;;,------------------------
;;| Avro Schema conversions
;;`------------------------
(s/def ::avro-ref
  (s/keys :req [:avro/name :avro/namespace]))
(s/def ::spec->avro-ref
  (s/map-of ::spec ::avro-ref))

(defn- ->avro-n+ns
  [spec]
  (m/assoc-some {:name (au/mangle (name *spec-k*))}
                :namespace (au/mangle (namespace *spec-k*))))

(defn- ->avro-ref
  [spec]
  (-> (->avro-n+ns spec) (qualify-keys :avro)))
(defn- avro-ref->avro-namespaced-name
  "Convert an `::avro-ref` to an Avro namespaced name used to reference an
  previously defined Avro `:type`"
  [avro-ref]
  (let [n  (:avro/name avro-ref)
        ns (:avro/namespace avro-ref)]
    (if ns (str ns "." n) n)))

(s/fdef assoc-avro-ref
  :args (s/cat :spec->avro-ref ::spec->avro-ref
               :spec ::spec))
(defn- assoc-avro-ref
  "Assocs an `::avro-ref` computed form `spec` in a `::spec->avro-ref`.

  To be called after defining an Avro type so that subsequent calls will just
  use its fully qualified name."
  [spec->avro-ref spec]
  (assoc spec->avro-ref *spec-k* (->avro-ref spec)))

(s/fdef ->avro-schema*
  :args (s/cat :spec->avro-ref ::spec->avro-ref
               :spec ::spec))
(defmulti ->avro-schema*
  "Returns a tuple: [`::spec->avro-ref` `::avro-schema`].

  The `::spec->avro-ref` map is updated in any calls that define Avro named
  types, so that schemas are only defined on the first instance and then
  referred to by fully qualified name.

  NOTE: you shouldn't need to call this directly, but you can extend this
  multimethod to implement schema generation for specs that aren't included
  here."
  (fn [spec->avro-ref spec]
    (if (get spec->avro-ref *spec-k*)
      ::avro-ref
      (spec-dispatch spec))))

(defmethod ->avro-schema* ::spec-k
  [spec->avro-ref spec]
  (binding [*spec-k* spec]
    (->avro-schema* spec->avro-ref (s/form spec))))

(defmethod ->avro-schema* ::avro-ref
  [spec->avro-ref spec]
  [spec->avro-ref (-> spec->avro-ref
                      (get *spec-k*)
                      avro-ref->avro-namespaced-name)])

(defmethod ->avro-schema* ::enum
  [spec->avro-ref spec]
  [(assoc-avro-ref spec->avro-ref spec)
   (merge (->avro-n+ns spec)
          {:symbols spec
           :type    :enum})])

(defmethod ->avro-schema* 'clojure.spec.alpha/with-gen
  [spec->avro-ref spec]
  (->avro-schema* spec->avro-ref (second spec)))

(defmacro ^:private defmethods-pred->primitive-avro-schema
  [pred->avro-type]
  `(do
     ~@(for [[p t] pred->avro-type
             :let  [ps (symbol "clojure.core" (str p))]]
         `(defmethod ->avro-schema* '~ps
            [spec->avro-ref# _#]
            [spec->avro-ref# {:type ~t}]))))
(defmethods-pred->primitive-avro-schema
  {nil?     :null
   boolean  :boolean
   boolean? :boolean
   short    :int
   ;; TODO shorts, short-array
   int      :int
   ;; TODO ints, int-array
   int?     :long
   pos-int? :long
   neg-int? :long
   nat-int? :long
   long     :long
   ;; TODO longs, long-array
   float    :float
   ;; TODO floats, float-array
   float?   :double
   double   :double
   ;; TODO doubles, double-array
   double?  :double
   str      :string
   string?  :string})

(defmethod ->avro-schema* 'scribe.decimal/->decimal-conformer
  [spec->avro-ref spec]
  (let [{:keys [precision scale]} (kwargs->m (rest spec))]
    [spec->avro-ref {:type        :bytes
                     :logicalType :decimal
                     :precision   precision
                     :scale       scale}]))

(defmacro ^:private defmethods-pred->logical-avro-schema
  [pred->avro-type+logicalType]
  `(do
     ~@(for [[p [t lt]] pred->avro-type+logicalType]
         `(defmethod ->avro-schema* '~p
            [spec->avro-ref# _#]
            [spec->avro-ref# {:type ~t
                              :logicalType ~lt}]))))
(defmethods-pred->logical-avro-schema
  {clojure.core/inst?           [:long :timestamp-millis]
   scribe.time.core/instant?    [:long :timestamp-millis]
   scribe.time.core/local-date? [:int :date]
   scribe.time.core/local-time? [:int :time-millis]})

(defmethod ->avro-schema* 'clojure.spec.alpha/coll-of
  [spec->avro-ref spec]
  (let [;; TODO support opts ???
        pred (second spec)
        [spec->avro-ref
         items-schema] (->avro-schema* spec->avro-ref pred)
        schema         {:items items-schema
                        :type  :array}]
    [spec->avro-ref schema]))

(defmethod ->avro-schema* 'clojure.spec.alpha/map-of
  [spec->avro-ref spec]
  (let [;; NOTE The avro schema specification does only allow string keys so we just
        ;; ignore kpred
        [_kpred vpred _opts]      (rest spec)
        [spec->avro-ref v-schema] (->avro-schema* spec->avro-ref vpred)]
    [spec->avro-ref
     {:values v-schema
      :type   :map}]))

(defn- ->record-field
  [spec->avro-ref spec & {:keys [optional?]
                          :or   {optional? false}}]
  (let [[spec->avro-ref schema]
        (->avro-schema* spec->avro-ref spec)
        type (if optional? [:null schema] schema)]
    [spec->avro-ref (cond-> {:name (au/mangle (name spec))
                             :type type}
                      optional? (assoc :default nil))]))
(defmethod ->avro-schema* 'clojure.spec.alpha/keys
  [spec->avro-ref spec]
  (let [opts   (kwargs->m (rest spec))
        req-un (:req-un opts)
        opt-un (:opt-un opts)
        ->fields
        (fn [spec->avro-ref ks ->record-field]
          (reduce (fn [[spec->avro-ref field-schemas] spec]
                    (let [[spec->avro-ref field-schema]
                          (->record-field spec->avro-ref spec)]
                      [spec->avro-ref (conj field-schemas field-schema)]))
                  [spec->avro-ref []]
                  ks))
        [spec->avro-ref req-fields]
        (->fields spec->avro-ref req-un ->record-field)
        [spec->avro-ref opt-fields]
        (->fields spec->avro-ref
                  opt-un
                  #(->record-field %1 %2 :optional? true))]
    [(assoc-avro-ref spec->avro-ref spec)
     (merge (->avro-n+ns spec)
            {:fields (concat req-fields opt-fields)
             :type   :record})]))

(defmethod ->avro-schema* 'clojure.spec.alpha/or
  [spec->avro-ref spec]
  (let [preds (or-preds spec)]
    (reduce (fn [[spec->avro-ref members-schemas] spec]
              (let [[spec->avro-ref member-schema]
                    (->avro-schema* spec->avro-ref spec)]
                [spec->avro-ref (conj members-schemas member-schema)]))
            [spec->avro-ref []]
            preds)))

(defmethod ->avro-schema* 'clojure.spec.alpha/and
  [spec->avro-ref spec]
  (let [preds (rest spec)
        merge-spec (fn [s1 s2] (if (string? s2) s2 (merge s1 s2)))]
    (->> preds
         (reduce (fn [[spec->avro-ref avro-schema] p]
                   ;; FIXME Bug if an a-rfs is added, and later overridden
                   (if-let [[a-rfs a-s]
                            (try
                              (->avro-schema* spec->avro-ref p)
                              (catch Exception ex
                                (log/trace
                                 ex
                                 "catching ex in ->avro-schema (s/and), returning nil"
                                 (merge (ex-data ex)
                                        {:p                  p
                                         :avro-schema-so-far avro-schema
                                         :spec->avro-ref     spec->avro-ref
                                         :spec               spec
                                         :*spec-k*           *spec-k*
                                         :preds              preds}))
                                nil))]
                     [a-rfs (merge-spec avro-schema a-s)]
                     [spec->avro-ref avro-schema]))
                 [spec->avro-ref nil]))))

(defn- merge-avro-record-schemas
  [s1 s2]
  (if s1
    (update s1 :fields #(concat % (:fields s2)))
    s2))
(defmethod ->avro-schema* 'clojure.spec.alpha/merge
  [spec->avro-ref spec]
  (let [preds  (rest spec)
        ;; NOTE renames record to top-level name, ignoring the names of the
        ;; "inner" specs
        rename-record (fn [[a-rfs a-s]]
                        [a-rfs (merge a-s (->avro-n+ns spec))])]
    (->> preds
         (reduce (fn [[spec->avro-ref avro-schema] p]
                   (let [;; NOTE HACK dissocing the avro-ref corresponding to
                         ;;           *spec-k*, since right here, we're still
                         ;;           building the schema. we don't want
                         ;;           ->avro-schema* `s/keys calls to find it
                         ;;           and use it, instead of doing their job.
                         ;;           The last call will make an appropriate
                         ;;           avro-ref and assoc it
                         spec->avro-ref (dissoc spec->avro-ref *spec-k*)
                         [a-rfs a-s]    (->avro-schema* spec->avro-ref p)]
                     [a-rfs (merge-avro-record-schemas avro-schema a-s)]))
                 [spec->avro-ref nil])
         rename-record)))

(defmethod ->avro-schema* :default
  [spec->avro-ref spec]
  (throw (ex-info "could not determine Avro schema"
                  {:spec spec, :spec->avro-ref spec->avro-ref})))

(s/fdef ->avro-schema
  :args (s/cat :spec-k qualified-keyword?))
(defn ->avro-schema
  "Generates an Avro schema for `spec-k`.

  `spec-k`: the key of the Spec to convert"
  [spec-k]
  (second (->avro-schema* {} spec-k)))

;;,-----------------------
;;| Avro pre-serialization
;;`-----------------------
(s/fdef pre-serialize*
  :args (s/cat :spec ::spec
               :value-to-serialize any?))
(defmulti pre-serialize*
  "See `pre-serialize`.

  NOTE: you shouldn't need to call this directly, but you can extend this
  multimethod to implement value pre-serialization for specs that aren't
  included here."
  (fn [spec _v] (spec-dispatch spec)))

(defmethod pre-serialize* ::spec-k
  [spec v]
  (binding [*spec-k* spec]
    (pre-serialize* (s/form spec) v)))

(defmethod pre-serialize* 'clojure.spec.alpha/with-gen
  [spec v]
  (pre-serialize* (second spec) v))

(defmethod pre-serialize* `str
  [_ v]
  (str v))

(defmethod pre-serialize* 'scribe.decimal/->decimal-conformer
  [spec v]
  (let [{:keys [precision scale]} (kwargs->m (rest spec))]
    (decimal/decimal->bytes v precision scale)))

(defmethod pre-serialize* 'clojure.core/inst?
  [spec v]
  (t/to-epoch-milli v))

(defmethod pre-serialize* 'scribe.time.core/instant?
  [spec v]
  (t/to-epoch-milli v))

(defmethod pre-serialize* 'scribe.time.core/local-date?
  [spec v]
  (t/to-epoch-day v))

(defmethod pre-serialize* 'scribe.time.core/local-time?
  [spec v]
  (t/to-milli-of-day v))

(defmethod pre-serialize* 'clojure.spec.alpha/coll-of
  [spec v]
  (let [pred (second spec)]
    (map (partial pre-serialize* pred) v)))

(defmethod pre-serialize* 'clojure.spec.alpha/map-of
  [spec v]
  (let [;; NOTE the Avro spec only allows string keys, so we ignore the kpred...
        [_kpred vpred & _opts] (rest spec)]
    (m/map-vals (partial pre-serialize* vpred) v)))

(defmethod pre-serialize* 'clojure.spec.alpha/keys
  [spec v]
  (let [opts (kwargs->m (rest spec))
        ks   (concat (:opt-un opts) (:req-un opts))]
    (reduce (fn [m k-spec]
              (update-some m
                           (disqualify-keyword k-spec)
                           (partial pre-serialize* k-spec)))
            v
            ks)))

(defn- or-pred-for
  "Given an `or-spec-k` pointing to an `s/or` spec and a value `v`, it returns
  the matching pred for `v`"
  [or-spec-form v]
  (let [matching-k (first (s/conform *spec-k* v))
        k->pred    (kwargs->m (rest or-spec-form))]
    (k->pred matching-k)))
(defmethod pre-serialize* 'clojure.spec.alpha/or
  [spec v]
  (pre-serialize* (or-pred-for spec v) v))

(defmethod pre-serialize* 'clojure.spec.alpha/and [spec v]
  (let [preds (rest spec)]
    ;; if (empty? preds), return v
    ;; if all preds fail, return v
    ;; otherwise, apply (pre-serialize pred v) for all preds
    (reduce (fn [v p]
              (try
                (pre-serialize* p v)
                (catch Exception ex
                  (log/trace ex
                             "catching ex in pre-serialize*, returning v"
                             (merge (ex-data ex) {:v v, :p p}))
                  v)))
            v
            preds)))

(defmethod pre-serialize* 'clojure.spec.alpha/merge
  [spec v]
  (let [preds (rest spec)]
    (->> preds
         (reduce #(pre-serialize* %2 %1) v))))

(defmethod pre-serialize* :default [_ v] v)

(s/fdef pre-serialize
  :args (s/cat :spec-k qualified-keyword?
               :v any?))
(defn pre-serialize
  "Prepares a value `v` for encoding to Avro.

  For example, it converts `BigDecimal`s to bytes for Avro `:decimal`s,
  `:timestamp-millis` to integers and so on, according to the Avro
  specification."
  [spec-k v]
  (s/assert spec-k v)
  (pre-serialize* spec-k v))

;;,------------------------
;;| Avro post-deserialization
;;`------------------------
(s/fdef post-deserialize*
  :args (s/cat :spec ::spec
               :deserialized-value any?))
(defmulti post-deserialize*
  "See `post-deserialize`.

  NOTE: you shouldn't need to call this directly, but you can extend this
  multimethod to implement value post-deserialization for specs that aren't
  included here."
  (fn [spec _v] (spec-dispatch spec)))

(defmethod post-deserialize* ::spec-k
  [spec v]
  (binding [*spec-k* spec]
    (post-deserialize* (s/form spec) v)))

(defmethod post-deserialize* ::enum
  [_ v]
  (keyword v))

(defmethod post-deserialize* 'clojure.spec.alpha/with-gen
  [spec v]
  (post-deserialize* (second spec) v))

(defmethod post-deserialize* 'scribe.decimal/->decimal-conformer
  [spec v]
  (let [{:keys [precision scale]} (kwargs->m (rest spec))]
    ;; TODO implement fixed
    (decimal/bytes->decimal v precision scale)))

(defmethod post-deserialize* 'clojure.core/inst?
  [spec v]
  (t/of-epoch-milli t/*instant-class* v))

(defmethod post-deserialize* 'scribe.time.core/instant?
  [spec v]
  (t/of-epoch-milli t/*instant-class* v))

(defmethod post-deserialize* 'scribe.time.core/local-date?
  [spec v]
  (t/of-epoch-day t/*local-date-class* v))

(defmethod post-deserialize* 'scribe.time.core/local-time?
  [spec v]
  (t/of-milli-of-day t/*local-time-class* v))

(defmethod post-deserialize* 'clojure.spec.alpha/coll-of
  [spec v]
  (let [pred (second spec)]
    (map (partial post-deserialize* pred) v)))

(defmethod post-deserialize* 'clojure.spec.alpha/map-of
  [spec v]
  (let [;; NOTE the Avro spec only allows string keys, so we ignore the kpred...
        [_kpred vpred & _opts] (rest spec)]
    (m/map-vals (partial post-deserialize* vpred) v)))

(defmethod post-deserialize* 'clojure.spec.alpha/keys
  [spec v]
  (let [opts    (kwargs->m (rest spec))
        opt-un? (set (:opt-un opts))
        ks      (concat (:req-un opts) (:opt-un opts))]
    (reduce (fn [m k-spec]
              (let [k (disqualify-keyword k-spec)
                    opt-k-not-present?
                    (and (opt-un? k-spec) (nil? (get m k)))]
                (if opt-k-not-present?
                  ;; abracad assocs a nil, i don't want it because it messes up
                  ;; my tests U_U.
                  ;;
                  ;; if it was defined as an s/nilable ??? what do we do then,
                  ;; huh? everything is gonna explode, big time </s>
                  (dissoc m k)
                  (update-some m
                               (disqualify-keyword k-spec)
                               (partial post-deserialize* k-spec)))))
            v
            ks)))

(defn- or-avro-value-spec
  [or-spec-form v]
  ;; NOTE abracad puts a `:type` in the meta of deserialized unions so in case
  ;;      this is a Avro Record union, we try to get the predicate that way
  ;; NOTE Avro unions like [:decimal1 :fixed1] are not supported (won't have
  ;;      metadata, so it'd be way too magical to try and find matching pred)
  (let [preds    (or-preds or-spec-form)
        v-pred-k (-> v type keyword)]
    (->> preds
         (filter (partial = v-pred-k))
         first)))
(defmethod post-deserialize* 'clojure.spec.alpha/or
  [spec v]
  (if-let [sn (or-avro-value-spec spec v)]
    (post-deserialize* sn v)
    v))

(defmethod post-deserialize* 'clojure.spec.alpha/and [spec v]
  (let [preds (rest spec)]
    ;; if (empty? preds), return v
    ;; if all preds fail, return v
    ;; otherwise, apply (post-deserialize pred v) for all preds
    (reduce (fn [v p]
              (try
                (post-deserialize* p v)
                (catch Exception ex
                  (log/trace ex
                             "catching ex in post-deserialize*, returning v"
                             (merge (ex-data ex) {:v v, :p p}))
                  v)))
            v
            preds)))

(defmethod post-deserialize* 'clojure.spec.alpha/merge
  [spec v]
  (let [preds (rest spec)]
    (->> preds
         (reduce #(post-deserialize* %2 %1) v))))

(defmethod post-deserialize* :default [_ v] v)

(s/fdef post-deserialize
  :args (s/cat :spec-k qualified-keyword?
               :v any?))
(defn post-deserialize
  "Processes a deserailized value `v` from Avro.

  For example, it converts bytes to a `BigDecimal` for an Avro `:decimal`,
  integers to `Instants` for `:timestamp-millis` and so on, according to the
  Avro specification."
  [spec-k v]
  (post-deserialize* spec-k v))

;;,------
;;| SerDe
;;`------

(defn binary-encoded
  "See `abracad.avro/binary-encoded`."
  [spec-k & records]
  (let [s  (->avro-schema spec-k)
        rs (map (partial pre-serialize spec-k) records)]
    (apply aa/binary-encoded s rs)))

(defn decode
  "See `abracad.avro/decode`."
  [spec-k source]
  (let [s (->avro-schema spec-k)]
    (post-deserialize spec-k (aa/decode s source))))

;; TODO all all the abracads
