(ns scribe.utils
  "Generic utility functions."
  (:require [medley.core :refer [map-keys]]))

(defn disqualify-keyword
  "Removes the namespace from the keyword `k`."
  [k]
  (-> k
      name
      keyword))

(defn disqualify-keys
  "Removes the namespaces of the keys in `m`."
  [m]
  (map-keys disqualify-keyword m))

(defn qualify-keyword
  "Qualifies the keyword `k` with namespace `ns`."
  [ns k]
  (keyword (name ns) (name k)))

(defn qualify-keys
  "Qualifies the keys in `m` with `ns`."
  [m ns]
  (map-keys (partial qualify-keyword ns) m))

(defn ns=
  "Does the namespace of the keyword `k` equal `ns`?"
  [ns k]
  (= (name ns) (namespace k)))

(defn update-some
  "Updates a key `k` with a fn `f` in a map `m`, if and only if the value in the
  map is present."
  ([m k f] (if-not (find m k) m (update m k f)))
  ([m k f & kfs]
   (reduce (fn [m [k f]] (update-some m k f))
           (update-some m k f)
           (partition 2 kfs))))

(defn kwargs->m
  "Rolls `kwargs` into a plain-old map."
  [kwargs]
  (apply hash-map kwargs))
