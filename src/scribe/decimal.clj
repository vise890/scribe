(ns scribe.decimal
  "Utils for the conversions and massaging of BigDecimal values."
  (:require [abracad.avro :as aa]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as sgen]
            [scribe.time.utils :as u])
  (:import [java.math BigDecimal BigInteger MathContext]
           java.nio.ByteBuffer
           [org.apache.avro Conversions$DecimalConversion LogicalTypes]))

(defn set-scale
  "Sets the `scale` of a `BigDecimal` `bd`."
  [^BigDecimal bd scale]
  (.setScale bd scale))

;; TODO ??? use BigDecimal(BigInteger unscaledVal, int scale, MathContext mc) to set precision

(defn set-precision
  "Sets the `precision` of a `BigDecimal` `bd`."
  [^BigDecimal bd precision]
  (let [prec (+ (.scale bd)
                (- precision (.precision bd)))]
    (.setScale bd prec)))

(defn- set-precision-scale
  "Best attempt to set the `precision` and `scale` of a `BigDecimal` `bd`."
  [bd precision scale]
  (-> bd
      (set-precision precision)
      (set-scale scale)))

(defn s-conform-decimal
  "Conforms the `decimal?` `bd` with `precision` and `scale`, returning
  `:clojure.spec.alpha/invalid` if unsuccessful.

  This is the `f` in the call to `clojure.spec.alpha/conformer`.

  NOTE you probably don't want to call this, use `->decimal-conformer` instead."
  [precision scale bd]
  (if-not (decimal? bd)
    :clojure.spec.alpha/invalid
    (try
      (set-precision-scale bd precision scale)
      (catch ArithmeticException _
        :clojure.spec.alpha/invalid))))

(defmacro ->decimal-conformer
  "Returns a `clojure.spec.alpha/conformer`, that conforms a `decimal?` with
  `precision` and `scale`, returning `:clojure.spec.alpha/invalid` if
  unsuccessful."
  [& {:keys [precision scale]}]
  `(s/with-gen
     (s/conformer (partial s-conform-decimal ~precision ~scale))
     (fn []
       (sgen/fmap (fn [i#]
                    (BigDecimal. (BigInteger. (str i#))
                                 (int ~scale)
                                 (MathContext. ~precision)))
                  (sgen/large-integer)))))

(defn- ->LogicalType
  ^org.apache.avro.LogicalType
  [precision scale]
  (LogicalTypes/decimal (int precision) (int scale)))

(defn- ->avro-schema
  ^org.apache.avro.Schema
  [type precision scale]
  (aa/parse-schema
   {:type        type
    :logicalType :decimal
    :precision   precision
    :scale       scale}))

(defn decimal->bytes
  "Converts a `BigDecimal` `bd` to a `ByteBuffer` containing the
  two's-complement representation of its underlying unscaled `BigInteger` value
  in big-endian byte order."
  [bd precision scale]
  (.toBytes (Conversions$DecimalConversion.)
            (set-precision-scale bd precision scale)
            (->avro-schema :bytes precision scale)
            (->LogicalType precision scale)))

(defn decimal->fixed
  "Converts a `BigDecimal` to a `org.apache.avro.generic.GenericFixed`
  containing the two's-complement representation of its underlying unscaled
  BigInteger value in big-endian byte order.

  TODO FIXME not implemented"
  [bd precision scale]
  (u/not-implemented!)
  (.toFixed (Conversions$DecimalConversion.)
            (set-scale bd scale)
            (->avro-schema :fixed precision scale)
            (->LogicalType precision scale)))

(defn byte-buffer?
  "Is `x` an instance of a `java.nio.ByteBuffer` or any of its subclasses?"
  [x]
  (isa? (class x) java.nio.ByteBuffer))

(defn- ->buf
  "Half-assed cast to buffer if needed."
  ^ByteBuffer
  [buf-or-bytes]
  (if (byte-buffer? buf-or-bytes)
    buf-or-bytes
    (ByteBuffer/wrap buf-or-bytes)))

(defn bytes->decimal
  "Converts a byte array or `ByteBuffer` containing the two's-complement
  representation of an unscaled Integer value in big-endian byte
  order to a `BigDecimal`."
  [buffer-or-bytes precision scale]
  (.fromBytes (Conversions$DecimalConversion.)
              (->buf buffer-or-bytes)
              (->avro-schema :bytes precision scale)
              (->LogicalType precision scale)))

(defn fixed->decimal
  "Converts an `org.apache.avro.generic.GenericFixed` containing the
  two's-complement representation of an unscaled Integer value in big-endian
  byte order to a `BigDecimal`.

  TODO FIXME not really implemented"
  [^org.apache.avro.generic.GenericFixed fixed precision scale]
  (u/not-implemented!)
  (.fromFixed (Conversions$DecimalConversion.)
              fixed
              (->avro-schema :fixed precision scale)
              (->LogicalType precision scale)))
