(ns scribe.time.core
  "Multimethod definitions for generically dealing with different
  implementations of time.")

(def
  ^{:dynamic true
    :doc
    "The concrete class for representing `instant?`s.

  Defaults to `java.time.Instant`."}
  *instant-class*
  java.time.Instant)

(def
  ^{:dynamic true
    :doc
    "The default concrete class for representing `local-date?`s.

  Defaults to `java.time.LocalDate`."}
  *local-date-class*
  java.time.LocalDate)

(def
  ^{:dynamic true
    :doc
    "The default concrete class for representing `local-time?`s.

  Defaults to `java.time.LocalTime`."}
  *local-time-class*
  java.time.LocalTime)


;;,--------
;;| Instant
;;`--------

;; https://avro.apache.org/docs/1.8.2/spec.html#Timestamp+%28millisecond+precision%29

;; The timestamp-millis logical type represents an instant on the global
;; timeline, independent of a particular time zone or calendar, with a precision
;; of one millisecond.

;; A timestamp-millis logical type annotates an Avro long, where the long stores
;; the number of milliseconds from the unix epoch, 1 January 1970 00:00:00.000
;; UTC. Timestamp (microsecond precision)


;; https://avro.apache.org/docs/1.8.2/spec.html#Timestamp+%28microsecond+precision%29

;; The timestamp-micros logical type represents an instant on the global
;; timeline, independent of a particular time zone or calendar, with a precision
;; of one microsecond.

;; A timestamp-micros logical type annotates an Avro long, where the long stores
;; the number of microseconds from the unix epoch, 1 January 1970
;; 00:00:00.000000 UTC.


;; https://docs.oracle.com/javase/8/docs/api/java/time/Instant.html

;; An instantaneous point on the time-line.

;; This class models a single instantaneous point on the time-line. This might
;; be used to record event time-stamps in the application.

;; The range of an instant requires the storage of a number larger than a long.
;; To achieve this, the class stores a long representing epoch-seconds and an
;; int representing nanosecond-of-second, which will always be between 0 and
;; 999,999,999. The epoch-seconds are measured from the standard Java epoch of
;; 1970-01-01T00:00:00Z where instants after the epoch have positive values, and
;; earlier instants have negative values. For both the epoch-second and
;; nanosecond parts, a larger value is always later on the time-line than a
;; smaller value

(defmulti instant?
  "Does the arg represent a single instant in time?

  Pretty much equivalent to clojure.core/inst?"
  type)
(defmethod instant? :default [_] false)
(defmethod instant? *instant-class* [_] true)

;; TODO explicit s/conformer for millis
;; TODO explicit s/conformer for micros

(defmulti to-epoch-milli
  "Converts instant to the number of milliseconds from epoch."
  type)
(defmulti to-epoch-micro
  "Converts instant to the number of microseconds from epoch." type)
(defmulti of-epoch-milli
  "Converts a number of milliseconds from epoch to an instance of target-class."
  (fn [target-class _millis] target-class))
(defmulti of-epoch-micro
  "Converts a number of microseconds from epoch to an instance of target-class."
  (fn [target-class _micros] target-class))

;;,----------
;;| LocalDate
;;`----------

;; https://avro.apache.org/docs/1.8.2/spec.html#Date

;; The date logical type represents a date within the calendar, with no
;; reference to a particular time zone or time of day.

;; A date logical type annotates an Avro int, where the int stores the number of
;; days from the unix epoch, 1 January 1970 (ISO calendar).


;; https://docs.oracle.com/javase/8/docs/api/java/time/LocalDate.html

;; A date without a time-zone in the ISO-8601 calendar system, such as
;; 2007-12-03.

;; LocalDate is an immutable date-time object that represents a date, often
;; viewed as year-month-day. Other date fields, such as day-of-year, day-of-week
;; and week-of-year, can also be accessed. For example, the value "2nd October
;; 2007" can be stored in a LocalDate.

;; This class does not store or represent a time or time-zone. Instead, it is a
;; description of the date, as used for birthdays. It cannot represent an
;; instant on the time-line without additional information such as an offset or
;; time-zone.

;; The ISO-8601 calendar system is the modern civil calendar system used today
;; in most of the world. It is equivalent to the proleptic Gregorian calendar
;; system, in which today's rules for leap years are applied for all time. For
;; most applications written today, the ISO-8601 rules are entirely suitable.
;; However, any application that makes use of historical dates, and requires
;; them to be accurate will find the ISO-8601 approach unsuitable.

(defmulti local-date?
  "Does the arg represent a calendar date, without referencing a time zone or a
  time of day?"
  type)
(defmethod local-date? :default [_] false)
(defmethod local-date? *local-date-class* [_] true)

(defmulti to-epoch-day
  "Converts a local date to the number of days since epoch."
  type)
(defmulti of-epoch-day
  "Converts a number of days since epoch to an instance of target-class."
  (fn [target-class _days] target-class))

;;,----------
;;| LocalTime
;;`----------

;; https://avro.apache.org/docs/1.8.2/spec.html#Time+%28millisecond+precision%29

;; The time-millis logical type represents a time of day, with no reference to a
;; particular calendar, time zone or date, with a precision of one millisecond.

;; A time-millis logical type annotates an Avro int, where the int stores the
;; number of milliseconds after midnight, 00:00:00.000. Time (microsecond
;; precision)


;; https://avro.apache.org/docs/1.8.2/spec.html#Time+%28microsecond+precision%29

;; The time-micros logical type represents a time of day, with no reference to a
;; particular calendar, time zone or date, with a precision of one microsecond.

;; A time-micros logical type annotates an Avro long, where the long stores the
;; number of microseconds after midnight, 00:00:00.000000.


;; https://docs.oracle.com/javase/8/docs/api/java/time/LocalTime.html

;; A time without a time-zone in the ISO-8601 calendar system, such as 10:15:30.

;; LocalTime is an immutable date-time object that represents a time, often
;; viewed as hour-minute-second. Time is represented to nanosecond precision.
;; For example, the value "13:45.30.123456789" can be stored in a LocalTime.

;; This class does not store or represent a date or time-zone. Instead, it is a
;; description of the local time as seen on a wall clock. It cannot represent an
;; instant on the time-line without additional information such as an offset or
;; time-zone.

;; The ISO-8601 calendar system is the modern civil calendar system used today
;; in most of the world. This API assumes that all calendar systems use the same
;; representation, this class, for time-of-day.

(defmulti local-time?
  "Does the arg represent a time of day, without referencing a time zone or a
  date?"
  type)
(defmethod local-time? :default [_] false)
(defmethod local-time? *local-time-class* [_] true)

;; TODO explicit s/conformer for millis
;; TODO explicit s/conformer for micros

(defmulti to-milli-of-day
  "Converts a local time to the number of milliseconds since midnight."
  type)
(defmulti to-micro-of-day
  "Converts a local time to the number of microseconds since midnight."
  type)
(defmulti of-milli-of-day
  "Converts a number of milliseconds from midnight to an instance of
  target-class."
  (fn [target-class _millis] target-class))
(defmulti of-micro-of-day
  "Converts a number of microseconds from midnight to an instance of
  target-class."
  (fn [target-class _micros] target-class))

;;,----------------------------
;;| Java Period / Avro Duration
;;`----------------------------

;; https://avro.apache.org/docs/1.8.2/spec.html#Duration

;; The duration logical type represents an amount of time defined by a number of
;; months, days and milliseconds. This is not equivalent to a number of
;; milliseconds, because, depending on the moment in time from which the
;; duration is measured, the number of days in the month and number of
;; milliseconds in a day may differ. Other standard periods such as years,
;; quarters, hours and minutes can be expressed through these basic periods.

;; A duration logical type annotates Avro fixed type of size 12, which stores
;; three little-endian unsigned integers that represent durations at different
;; granularities of time. The first stores a number in months, the second stores
;; a number in days, and the third stores a number in milliseconds.


;; https://docs.oracle.com/javase/8/docs/api/java/time/Period.html

;; A date-based amount of time in the ISO-8601 calendar system, such as '2
;; years, 3 months and 4 days'.

;; This class models a quantity or amount of time in terms of years, months and
;; days. See Duration for the time-based equivalent to this class.

;; Durations and Periods differ in their treatment of daylight savings time when
;; added to ZonedDateTime. A Duration will add an exact number of seconds, thus
;; a duration of one day is always exactly 24 hours. By contrast, a Period will
;; add a conceptual day, trying to maintain the local time.

;; For example, consider adding a period of one day and a duration of one day to
;; 18:00 on the evening before a daylight savings gap. The Period will add the
;; conceptual day and result in a ZonedDateTime at 18:00 the following day. By
;; contrast, the Duration will add exactly 24 hours, resulting in a
;; ZonedDateTime at 19:00 the following day (assuming a one hour DST gap).

;; The supported units of a period are YEARS, MONTHS and DAYS. All three fields
;; are always present, but may be set to zero.

;; The ISO-8601 calendar system is the modern civil calendar system used today
;; in most of the world. It is equivalent to the proleptic Gregorian calendar
;; system, in which today's rules for leap years are applied for all time.

;; The period is modeled as a directed amount of time, meaning that individual
;; parts of the period may be negative.

;; TODO ??? not implemented yet, different representations are hard to reconcile
;; (defmulti period?
;;   "Does the arg represent a period of time?"
;;   type)
;; (defmethod period? :default [_] false)

;; (defmulti to-<days+months+millis>
;;   "Converts a period to a tuple of days+months+millis."
;;   type)
;; (defmulti of-<days+months+millis>
;;   "Converts a tuple of days+months+millis to an instance of target-class."
;;   (fn [target-class days+months+millis] target-class))

;; TODO start+duration->period ?

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; INCLUDED IMPLEMENTATIONS ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmacro ^:private when-class [class-name & body]
  `(try
     (Class/forName ^String ~class-name)
     ~@body
     (catch ClassNotFoundException _#)))

(when-class "java.util.Date"
  (load "java_util_date"))

(when-class "java.sql.Timestamp"
  (load "java_sql_timestamp"))

(load "java_time")

(load "primitives")

(when-class "org.joda.time.Instant"
  (load "joda_time"))
