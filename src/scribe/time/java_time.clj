(in-ns 'scribe.time.core)
(clojure.core/require '[clojure.core :refer :all])
(require '[scribe.time.utils :as u])
(set! *warn-on-reflection* true)

(defn- not-implemented!
  "Throws UnsupportedOperationException to mark a code path as not implemented."
  []
  (throw (UnsupportedOperationException. "not implemented")))

(defmethod instant? java.time.Instant [_] true)

(defmethod to-epoch-milli java.time.Instant
  [^java.time.Instant i]
  (.toEpochMilli i))

(defmethod of-epoch-milli java.time.Instant
  [_ ms]
  (java.time.Instant/ofEpochMilli ms))

(defmethod to-epoch-micro java.time.Instant
  [^java.time.Instant i]
  ;; TODO implement me
  #_(+ (secons->micros (.getLong i))
       (nanos->micros! (.getNano i)))
  (not-implemented!))

(defmethod of-epoch-micro java.time.Instant
  [_ µs]
  (not-implemented!))

(defmethod local-date? java.time.LocalDate [_] true)

(defmethod to-epoch-day java.time.LocalDate
  [^java.time.LocalDate ld]
  (.toEpochDay ld))

(defmethod of-epoch-day java.time.LocalDate
  [_ days]
  (java.time.LocalDate/ofEpochDay days))

(defmethod local-time? java.time.LocalTime [_] true)

(defmethod to-milli-of-day java.time.LocalTime
  [^java.time.LocalTime lt]
  (-> lt .toNanoOfDay u/nanos->millis!))

(defmethod of-milli-of-day java.time.LocalTime
  [_ ms]
  (java.time.LocalTime/ofNanoOfDay (u/millis->nanos ms)))

(defmethod to-micro-of-day java.time.LocalTime
  [^java.time.LocalTime lt]
  (-> lt .toNanoOfDay u/nanos->micros!))

(defmethod of-micro-of-day java.time.LocalTime
  [_ µs]
  (java.time.LocalTime/ofNanoOfDay (u/micros->nanos µs)))
