(ns scribe.time.utils
  "Time conversion utils."
  (:import java.util.concurrent.TimeUnit))

(defn not-implemented!
  "Throws UnsupportedOperationException to mark a code path as not implemented."
  []
  (throw (UnsupportedOperationException. "not implemented")))

(defn millis->nanos
  "Converts milliseconds to nanoseconds."
  [ms] (.toNanos (TimeUnit/MILLISECONDS) ms))

(defn micros->nanos
  "Converts microseconds to nanoseconds."
  [µs]
  (.toNanos (TimeUnit/MICROSECONDS) µs))

(defn nanos->millis!
  "Converts nanoseconds to milliseconds, lossily."
  [ns] (.toMillis (TimeUnit/NANOSECONDS) ns))

(defn nanos->micros!
  "Converts nanoseconds to microseconds, lossily."
  [ns] (.toMicros (TimeUnit/NANOSECONDS) ns))
