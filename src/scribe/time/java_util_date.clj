(in-ns 'scribe.time.core)
(clojure.core/require '[clojure.core :refer :all])
(require '[scribe.time.utils :as u])
(set! *warn-on-reflection* true)

(defmethod instant? java.util.Date [_] true)

(defmethod to-epoch-milli java.util.Date
  [^java.util.Date d]
  (.getTime d))

(defmethod of-epoch-milli java.util.Date
  [_ ^long ms]
  (java.util.Date. ms))
