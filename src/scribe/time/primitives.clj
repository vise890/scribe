(in-ns 'scribe.time.core)
(clojure.core/require '[clojure.core :refer :all])
(require '[scribe.time.utils :as u])
(set! *warn-on-reflection* true)

(defmethod instant? java.lang.Long [_] true)

(defmethod to-epoch-milli java.lang.Long
  [^java.lang.Long ms]
  ms)

(defmethod of-epoch-milli java.lang.Long
  [_ ms]
  ms)

(defmethod to-epoch-micro java.lang.Long
  [^java.lang.Long µs]
  µs)

(defmethod of-epoch-micro java.lang.Long
  [_ µs]
  µs)

(defmethod local-date? java.lang.Integer [_] true)

(defmethod to-epoch-day java.lang.Integer
  [^java.lang.Integer epoch-day]
  epoch-day)

(defmethod of-epoch-day java.lang.Integer
  [_ epoch-day]
  epoch-day)

(defmethod local-date? java.lang.Long [_] true)

(defmethod to-epoch-day java.lang.Long
  [^java.lang.Long epoch-day]
  epoch-day)

(defmethod of-epoch-day java.lang.Long
  [_ epoch-day]
  epoch-day)

(defmethod local-time? java.lang.Integer [_] true)

(defmethod to-milli-of-day java.lang.Integer
  [^java.lang.Integer ms]
  ms)

(defmethod of-milli-of-day java.lang.Integer
  [_ ms]
  ms)

(defmethod local-time? java.lang.Long [_] true)

(defmethod to-milli-of-day java.lang.Long
  [^java.lang.Long ms]
  ms)

(defmethod of-milli-of-day java.lang.Long
  [_ ms]
  ms)

(defmethod to-micro-of-day java.lang.Long
  [^java.lang.Long µs]
  µs)

(defmethod of-micro-of-day java.lang.Long
  [_ µs]
  µs)
