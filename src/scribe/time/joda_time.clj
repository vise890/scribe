(in-ns 'scribe.time.core)
(clojure.core/require '[clojure.core :refer :all])
(set! *warn-on-reflection* true)

(defn- not-implemented!
  "Throws UnsupportedOperationException to mark a code path as not implemented."
  []
  (throw (UnsupportedOperationException. "not implemented")))

(def ^org.joda.time.DateTimeZone ^:private utc
  (org.joda.time.DateTimeZone/UTC))

(defmethod instant? org.joda.time.DateTime [_] true)

(defmethod to-epoch-milli org.joda.time.DateTime
  [^org.joda.time.DateTime dt]
  (-> dt (.toDateTime utc) .toInstant .getMillis))

(defmethod of-epoch-milli org.joda.time.DateTime
  [_ ^long ms]
  (-> ms org.joda.time.Instant. (org.joda.time.DateTime. utc)))

(defmethod instant? org.joda.time.Instant [_] true)

(defmethod to-epoch-milli org.joda.time.Instant
  [^org.joda.time.Instant i]
  (.getMillis i))

(defmethod of-epoch-milli org.joda.time.Instant
  [_ ^long ms]
  (org.joda.time.Instant. ms))

(defmethod local-date? org.joda.time.LocalDate [_] true)

(defmethod to-epoch-day org.joda.time.LocalDate
  [^org.joda.time.LocalDate ld]
  (let [ld (java.time.LocalDate/of (.getYear ld)
                                   (.getMonthOfYear ld)
                                   (.getDayOfMonth ld))]
    (.toEpochDay ld)))

(defmethod of-epoch-day org.joda.time.LocalDate
  [_ days]
  (let [ld (java.time.LocalDate/ofEpochDay days)]
    (org.joda.time.LocalDate. (.getYear ld)
                              (.getMonthValue ld)
                              (.getDayOfMonth ld))))

(defmethod local-time? org.joda.time.LocalTime [_] true)

(defmethod to-milli-of-day org.joda.time.LocalTime
  [^org.joda.time.LocalTime lt]
  (.getMillisOfDay lt))

(defmethod of-milli-of-day org.joda.time.LocalTime
  [_ ms]
  (-> (org.joda.time.LocalTime. (int 0) (int 0))
      (.plusMillis ms)))
