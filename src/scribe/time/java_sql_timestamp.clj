(in-ns 'scribe.time.core)
(clojure.core/require '[clojure.core :refer :all])
(require '[scribe.time.utils :as u])
(set! *warn-on-reflection* true)

(defmethod instant? java.sql.Timestamp [_] true)

(defmethod to-epoch-milli java.sql.Timestamp
  [^java.sql.Timestamp i]
  (inst-ms i))

(defmethod of-epoch-milli java.sql.Timestamp
  [_ ms]
  (java.sql.Timestamp. ms))
