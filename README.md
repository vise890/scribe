# scribe [![pipeline status](https://gitlab.com/vise890/scribe/badges/master/pipeline.svg)](https://gitlab.com/vise890/scribe/commits/master)

Schema transformation engine for clojure.spec, Avro, ...

**NOTE**: this library is experimental, use at your own risk!

## [API Docs](https://vise890.gitlab.io/scribe/)

## Usage

[![Clojars
Project](https://img.shields.io/clojars/v/vise890/scribe.svg)](https://clojars.org/vise890/scribe)

```clojure
;;; project.clj
[vise890/scribe "2018.11.26"]
```

```clojure
;; Start by requiring scribe.avro.core and defining a spec:
(require '[scribe.avro.core :refer :all])
(s/def :my.ns/string string?)

;; Use ->avro-schema to generate an Avro schema from the spec:
(example

 (->avro-schema :my.ns/string)

 {:type :string})


;; Some Avro types require names and optionally namespaces; Scribe uses the Spec
;; key to derive them:
(s/def :my.ns/enum #{:beep :boop})
(example

 (->avro-schema :my.ns/enum)

 {:name      "enum",
  :namespace "my.ns",
  :symbols   #{:boop :beep},
  :type      :enum})


;; Avro decimals require explicit scale and precision; Scribe provides a
;; conformer:
(require '[scribe.decimal :as decimal])
(s/def :my.ns/decimal
  (s/and decimal?
         (decimal/->decimal-conformer :scale 3
                                      :precision 5)))
(example

 (->avro-schema :my.ns/decimal)

 {:type :bytes,
  :logicalType :decimal,
  :precision 5, :scale 3})


;; There are also helpers for time in scribe.time (see below)


;; Most Spec forms are supported:
(s/def :my.ns/nonBlankString
  (s/and string?
         (complement string/blank?)))

(s/def :my.ns/collOf
  (s/coll-of string?))

(s/def :my.ns/recordOne
  (s/keys :req-un
          [:my.ns/nonBlankString]))

(s/def :my.ns/recordTwo
  (s/keys :req-un
          [:my.ns/recordOne]))

(s/def :my.ns/or
  (s/or :r1 :my.ns/recordOne
        :r2 :my.ns/recordTwo))

(example

 (->avro-schema :my.ns/or)

 [{:name "recordOne",
   :namespace "my.ns",
   :type :record,
   :fields [{:name "nonBlankString",
             :type {:type :string}}]}
  {:name "recordTwo",
   :namespace "my.ns",
   :type :record,
   :fields [{:name "recordOne",
             :type "my.ns.recordOne"}]}])


;; You can then use binary-encoded to serialize to bytes:
(def record1 {:nonBlankString "x7CtK"})
(def record2 {:recordOne record1})
(binary-encoded :my.ns/or record1)
(binary-encoded :my.ns/or record2)

;; .. and decode to go back:
(decode :my.ns/or (binary-encoded :my.ns/or record1))
;; note that it takes care of serializing decimals to bytes


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Time.

;; First require the time helpers:
(require '[scribe.time.core :as t])

;; `t/instant?`s are instantaneous points on the time-line.

;; Several underlying classes are supported:
(example

 (t/instant? (java.time.Instant/now))

 true)

(example

 (t/instant? (System/currentTimeMillis))

 true)

;; org.joda.time.DateTime, java.sql.Timestamp, and java.util.Date are also
;; supported. You can add your own by implementing the `t/instant?` multimethod.

;; After defining a spec, you can generate an Avro Schema:
(s/def :my.ns/instant t/instant?)
(example

 (->avro-schema :my.ns/instant)

 {:type :long, :logicalType :timestamp-millis})


;; You can binary encode instants from several classes:
(binary-encoded :my.ns/instant (System/currentTimeMillis))
(binary-encoded :my.ns/instant (java.time.Instant/now))

;; When deserializing, java.time.Instant is used by default:
(example

 (->> (binary-encoded :my.ns/instant 0)
      (decode :my.ns/instant))

 java.time.Instant/EPOCH)


;; You can override the concrete representation by binding `t/*instant-class*`:
(example

 (let [encoded (binary-encoded :my.ns/instant 0)]
   (binding [t/*instant-class* java.lang.Long]
     (decode :my.ns/instant encoded)))

 0)

;; There are similar facilities for representing Local Times and Local Dates:
t/local-time? t/*local-time-class*
t/local-date? t/*local-date-class*
```

## Name Mangling

Scribe uses `abracad.avro.util/*mangle-names*` to decide whether or not to
mangle names containing hyphens (`-`). Mangled names are Avro namespaces, type
names, and field names. See [the abracad
documentation](https://github.com/damballa/abracad#basic-deserialization) for
more info.

## License

Copyright © 2017 Martino Visintin

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
