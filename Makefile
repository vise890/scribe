k8s_openapi_url = https://raw.githubusercontent.com/kubernetes/kubernetes/master/api/openapi-spec/swagger.json

.PHONY: default kubernetes_download_openapi_spec

default:

kubernetes_download_openapi_spec:
	wget $(k8s_openapi_url) --output-document ./test/resources/k8s_openapi.json
