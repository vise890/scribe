x (A) pre-serialize for coll-of
x (A) pre-serialize for keys
x sets => enums
x (A) unions!! (required for #enum-by-ref)
x (A) enums are only defined in-line on the first occurrence (avro schema) #enum-by-ref
x or => union
x (C) all types update the registry to refer to types by ns+n after definition
x (A) scribe spec -> clojure spec
x (A) enum: make naked keywords set valid enum
x (A) decimal: make scale and precision optional, only required for avro
x (A) avro: pre-ser/post-des for union || just fail horribly and let users down big time
x move to qualified kw for spec names, clj spec needs them and it's a good practice!
x figure out how the hell we're gonna do post deser of unions with conflicting representations (that don't have metadata set by abracad)
x (A) Avro timestamp-millis / joda date-time
x (A) Avro date / joda local-date
x (A) `s/conformer`s for decimal precision and scale, ??? HOW!? (s/conformer (partial precision= 4)) ???
x (A) keys : opt-un
x make ->avro-schema- pre-serialize- post-deserialize- public + docs to allow users to extend if needed
x (B) error handling . behave better when given invalid values to serde
x (A) !!! custom generator for decimal / fix such-that fail !!!!!!!!!
x (A) support s/with-gen
x (A) timestamp-millis
x (A) date
x (C) time-millis
x +in_scope +not_needed4mvp java8 : LocalTime<->millis
x +in_scope +not_needed4mvp joda-time : LocalTime<->millis
x +in_scope +not_needed4mvp pred: int? (java Long, int, Integer, Short, Byte) Avro long: 64-bit signed integer
x +in_scope +not_needed4mvp pred: double? (java Double): Avro double: double precision (64-bit) IEEE 754 floating-point number
x +in_scope +not_needed4mvp (s/conformer long) : Avro long: 64-bit signed integer
x +in_scope +not_needed4mvp (s/conformer int) : Avro int: 32-bit signed integer
x +in_scope +not_needed4mvp (s/conformer float) : Avro float: single precision (32-bit) IEEE 754 floating-point number
x +in_scope +not_needed4mvp (s/conformer double) : Avro double: double precision (64-bit) IEEE 754 floating-point number
x (B) support for kebab-cased-namespaces/and-keywords
