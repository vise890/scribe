(defproject vise890/scribe "2018.12.01"
  :description "Schema transformation engine for Spec, Avro. ..."

  :url "https://gitlab.com/vise890/scribe"

  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[camel-snake-kebab "0.4.0"]
                 [com.damballa/abracad "0.4.13" :exclusions [org.apache.avro/avro]]
                 [medley "1.0.0"]
                 [org.apache.avro/avro "1.8.2"]
                 [org.clojure/clojure "1.9.0"]
                 [org.clojure/spec.alpha "0.2.176"]
                 [org.clojure/tools.logging "0.4.1"]]

  :plugins [[lein-codox "0.10.5"]]

  :profiles {:dev {:dependencies [[ch.qos.logback/logback-classic "1.2.3"]
                                  [ch.qos.logback/logback-core "1.2.3"]
                                  [expound "0.7.1"]
                                  [fipp "0.6.13"]
                                  [joda-time/joda-time "2.10"]
                                  [org.clojure/test.check "0.10.0-alpha2"]]
                   :resource-paths ["dev/resources" "test/resources"]}

             :test [:dev]

             :ci {:deploy-repositories
                  [["clojars" {:url           "https://clojars.org/repo"
                               :username      :env
                               :password      :env
                               :sign-releases false}]]}})
